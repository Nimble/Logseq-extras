#!/bin/lua

-- These two are the variables you need to change:
-- Specially the folder one.

--
--  Read the file
--
--local path = io.open(".path", "r")
local path
--folder = path:read("*all")
--path:close()


-- Discriminatory strings
local sA = "public:: true\n"
local diario = "Journal"
local unready = "#nochitlakaj"

function replace(file, sA)
    --
    --  Read the file
    --
    local f = io.open(file, "r")
    local content = f:read("*all")
    f:close()

    --
    -- Edit the string
    --
    --content = string.gsub(sA, "Hello", "Hello, ")
    -- local Scontent = string.format("%s", content)
    --print(Scontent)
    --print(string.len(Scontent))

    -- Checks for duplicates and already published files
    nota=string.find(content, sA)
    if nota~=nil then
        print(file .. " > this note has already been published. Nothing to be done. 🌠 <")
    elseif string.find(content, unready) then
        print(file .. " > This note is not ready for publication yet ☄️☄️ <")
    else
        --
        -- Write it out
        --
        local f = io.open(file, "w")
        f:write(sA .. content)
        f:close()
        print(" 🌊 This has be set to public: "..file)
    end
end

function scandir(directory)
    local pfile = assert(io.popen(("find '%s' -mindepth 1 -maxdepth 1 -type d -printf '%%f\\0'"):format(directory), 'r'))
    local list = pfile:read('*a')
    pfile:close()

    local folders = {}

    for filename in string.gmatch(list, '[^%z]+') do
        table.insert(folders, filename)
    end

    return folders
end

function dirLookup(dir)
--local p = io.popen('find "'..dir..'" -type f')  --Open directory look for files, save data in p. By giving '-type f' as parameter, it returns all files.
local p = io.popen('ls '..dir)  --Open directory look for files, save data in p. By giving '-type f' as parameter, it returns all files.   
for file in p:lines() do                         --Loop through all files
    gecko=string.find(file, ".md")
    if string.match(file, diario) then
        print(file .. " > 💫 This is a Journal and it will not be set to public 🦤 <")
        elseif gecko~=nil then
            --print(file .. " in spot " ..gecko) --debug
            replace(dir..file, sA)
        else
        print(" 🍃 "..file.." <<<<<<<<<<<< This is not a markdown file it will be ignored")
    end
end
end

-- Execution
print("--------------------------------------------------------------------------------------\n This script will append on top the string: "..sA.." Which will make public all your markdown.md documents in the folder you specify next " .. "\n It will also avoid to publish any file that is named 'Journal'. \n--------------------------------------------------------------------------------------\n")
print("Insert the path of the folder (defaults to the current one) where your notes are with the last backslash included since this doesn't use LuaFileSystem: (uses a bit of gnu-core-utils)")
folder = io.read()

print("-----------------------------------------------------------------\n Directory to be used is: " .. folder .. "\n-----------------------------------------------------------------\n")
dirLookup(folder)
--scandir(folder)
