---
title: desarrollo de vida de Baltes
tags:
categories:
date: 2022-08-31
lastMod: 2022-08-31
---
Dura toda la vida

Es multidireccional (existen multiples formas neuronales)

Multidimensional (diferentes áreas y campos)

#influido por #biología #cultura #historia

Existe o impera una distribución y administración de recursos biólogicos

  + Ejemplo: La mayoría del consumo energético se va al [cerebro]({{< ref "/pages/cerebro" >}})

Tiene y usa principios de plasticidad cerebral
