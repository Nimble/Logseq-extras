---
title: Medición y evaluación psicológica
tags:
categories:
date: 2022-08-29
lastMod: 2022-09-26
---
#Porcentajes


  + Instrumento de medición............................40%

  + Examen (2)...................................................40%

    + Uno teorico

    + Otro teorico-practico

  + Prueba de rendimiento académico............10%

  + Redes Semanticas.......................................10%

Se busca medir articulos no inteligencia

  + Buscamos encontrar curvas sesgadas en las mediciones de aptitudes

[Redes semánticas]({{< ref "/pages/Redes semánticas" >}}) - Conocer el lenguaje de los sujetos para hacer instrumentos más complejos preciso y acoplados a cierta población o individuo.

1845 **Horace Mann** crea los primeros tests

Despues **Bloom** hace una clasificación de los objetivos de instrucción también conocido como la [Taxonomia de Bloom]({{< ref "/pages/Taxonomia de Bloom" >}})

  + [Respuestas]({{< ref "/pages/Respuestas" >}}) - Se refiere a las formas que estas pueden tomar dentro de el sujeto

  + [Pruebas de aprovechamiento]({{< ref "/pages/Pruebas de aprovechamiento" >}}) - Dos tipos: Ensayo y objetivos

    + Tipos de [Reactivos]({{< ref "/pages/Reactivos" >}}): Cada uno de estos mide cosas distintas y son optimos en distintos escenarios dependiendo de lo que se busque medir

Rechazo de hipotesis nula implica que la propuesta es correcta

Que algo sea [confiabilidad]({{< ref "/pages/confiabilidad" >}}) no quiere decir que sea validez

  + Se ven alteradas por el [error de medición]({{< ref "/pages/error de medición" >}}). Sin embargo hay formas de mitigarlo mediante [Modelos de disminución de error]({{< ref "/pages/Modelos de disminución de error" >}})

  + La [varianza]({{< ref "/pages/varianza" >}}) afecta esta [estadística]({{< ref "/pages/estadística" >}})

  + El [análisis factorial]({{< ref "/pages/análisis factorial" >}}) prueba si en realidad se mide lo que se pretende medir

r_ll - real e hipotetica - Aleatoriamente paralelas ?????

**Mejor hora para aplicar instrumentos es de 9:00 - 12:00 hrs** #tips
