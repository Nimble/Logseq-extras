---
title: Sistema Nervioso Central
tags:
categories:
date: 2022-10-02
lastMod: 2022-10-02
---
Esta protegido por huesos



## Encéfalo [cerebro]({{< ref "/pages/cerebro" >}})

  + #etimología Encéfalo: *Endo* (dentro) - *cefalo* (cabeza)

  + Talamo
  + Cuerpo calloso

    + Una gran unión de axones, es decir [sustancia blanca](**Sustancia blanca:** Es la fibra confromada por [axón]({{< ref "/pages/axón" >}})es
)

    + Une el hemisfrio derecho con el izquierdo

## Columna Vertebral
  + Conocida como Médula espinal

  + [Nervios](Nervios
) espinales

    + Son dos pares y raices que se conectan

    + Parte [sensitiva](633a3324-6b8a-4801-8c33-c5c05f0d9769) (in/link) y [motora](Ventral
) (out) - Por lo mismo se divide en **ramas** ventrales y dorsales

    + Ramas Ventral
es llevan al Sistema Nervioso Autónomo

  + Secciones

    + Plexo cervical

    + Plexo braquial - músculo ← nervios raquídeos

    + Plexo lumbar - abdomen y miembros inferiores

    + Plexo sacro ← nervio ciático - es el más largo
