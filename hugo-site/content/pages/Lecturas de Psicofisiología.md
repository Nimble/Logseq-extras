---
title: Lecturas de Psicofisiología
tags:
categories:
date: 2022-10-02
lastMod: 2022-10-02
---
Semir Zeki hace experimentos con monos y descubre células de codificación del color. Responden a la longitud de onda pero no al color. Hay otras que responden al color pero no a la longitud de onda. Solo al ser comparadas y correlacionada esta información se formab a la imagen y el color.
