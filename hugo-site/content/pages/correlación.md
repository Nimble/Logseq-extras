---
title: correlación
tags:
categories:
date: 2022-09-04
lastMod: 2022-09-25
---
No existen correlaciones perfectas 1:1 - esto es una evidencia de error

  + Puede ser causado por deseabilidad social

  + Sujetos contestan de la manera (deseada) o esperada

    + Existe una manipulación instrumental

Compara reactivos de una forma de matrix

  + Un reactivo con todos los demas y forma diagonales de "1" enmedio donde se compara con si mismo en la tabla

  + Significancia < .050

# **Spearman**

Usado en escalas ordinales

# **Pearson**

Escala intervalar (intervalos) - Muestras grandes (Más de 30 sujetos)

Formula:

  + $$τ_{xy}=\frac{N\sum XY-\left(\sum X\right)\cdot \left(\sum Y\right)\:}{\sqrt[2]{\left(N\sum X²-\left(\sum X\right)^2\right)\cdot \left(N\sum Y²-\left(\sum Y\right)^2\right)}}$$

  + X = valor de **test**

  + Y = valor de **retest**

  + N = número o tamaño de muestra (cantidad de mediciones) y **solo usar rangos**

# **Rho**

  + Ordinal por rangos (intervalos ordenados) (30-15 sujetos)

  + Para crear rangos se ordenan los resultados

  + Se le da a cada categorización un peso/orden/numero iniciando por el más bajo en 1

    + Si hay valores empatados el **valor ordinal** de estos se suma y se divide sobre la **cantidad de reactivos empatados**

  + Formula:

    + $$rho=1-\frac{6\sum d²\:}{N³-N}$$

    + d = discrepancia o diferencia dada por (x-y)

      + x = valor de **test** ordinal

      + y = valor de **retest** ordinal

      + Es decir d es la **diferencia** entre el **test** y el **retest**

    + N = número o tamaño de muestra (cantidad de mediciones) y **solo usar rangos**

<ins>Diferencia entre Rho y Pearson se debe a la categorización de los reactivos lo cual hace que os valores usados no sean reales sino aproximados</ins>
