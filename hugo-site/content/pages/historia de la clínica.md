---
title: historia de la clínica
tags:
categories:
date: 2022-09-23
lastMod: 2022-09-23
---
**Hipocrates** - lógica del bienestar y el cuidado de si mismo y otros

**Galeno** - cuidado del enfermo a través de la evitación o eliminación de la **enfermedad**

  + Suma el naturalismo de hipocrates al racionalismo

Siglo XVIII - _The plague doctors_ - se causalidades aleatorias y arbitrarias

Post Siglo XVIII - Médicos ligados a la curación

  + Capitalismo forza a la psicología a adquirir utilidad lo cual a su vez genera o insentiviza la creación de la psicología clínica y la psiquiatría

Emil Kraplein (1856-1926) clasifica los primeros transtornos psiquiatricos

Witmer (1907) acuña el termino psicología clínica

  + Enfatiza que el término clínico debe ser temporal

En 1879 nace formalmente la lógica **funcionalista**

  + Nació marcada sobre las influencias de la época con metas pragmaticas y utilitarias

  + Los modelos conductuales surgen a partir de este principio

Boulder Estados Unidos (1949) - Utilidad psicología y psiquiatría clínica en la postguerra por los transtornos originados de esta

La psicología clínica moderna se ve marcada por la relatividad

  + La conceptualización de los problemas mentales cambia a microrelatos contextualizados en vez de una perspectiva estandarizada hegemónica o única

La psicología y clínica esta relacionada y cambia su perspectiva completa conforme cambia la época. Es una interrelación con la sociedad.
