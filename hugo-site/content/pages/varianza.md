---
title: varianza
tags:
categories:
date: 2022-09-04
lastMod: 2022-09-04
---
Es la medida de dispersión que muestra como se distribuye la cantidad de los datos recabados

Solo analiza datos específicos

Varianza Error

  + Por [error de medición]({{< ref "/pages/error de medición" >}})

Varianza Especifica

  + Caracteristicas Particulares

Varianza Común

  + Grupos y caracteristicas comunes del mismo
