---
title: Método Epidemiológico
tags:
categories:
date: 2022-08-30
lastMod: 2022-09-04
---
Se basa en la relación **origen-causas**

Debido a como esta estructurado a menudo arroja una etiología del fenomeno o al menos se orilla a ello

Su fundamento principal es **entender** y **predecir** la **propagación** del fenomeno o padecimiento

Factores clave

  + Distribución y prevalencia de alguna ocurrencia o evento y sintomas

  + Frecuencia

  + Poblaciones

Intenta explicar el fenomeno y lo que le rodea (correlaciones) y causas del mismo

**Permite** diseñar programas de intervención con base a:

  + Factores de riesgo y protección

  + Vulnerabilidad (más interno o innato) - Tendencia que tiene la población a ocurrencias

Caracteristicas del proceso salud-enfermedad

  + Manuales diagnostivo como DSM-V

Describe historia natural de la enfermedad ↢ como se desarrolla - patología y sintomas

Tipos

  + Epidemología analítica: Busca relaciones

  + Epidemología Experimental: Explica factores

  + Ecoepidemiologia: Factores psicológicos que intervienen

  + Epidemología descriptiva: Detalla caracteristicas
