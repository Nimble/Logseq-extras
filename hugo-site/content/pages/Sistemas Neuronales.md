---
title: Sistemas Neuronales
tags:
categories:
date: 2022-10-02
lastMod: 2022-10-03
---
## Glosario #glosario

  + Función Psicométrica

    + Relación entre estímulos-sensaciones

  + Fisiología Sensorial

    + Consecuencia neuronal de estímulos (transducción al lenguaje cerebral)

## Subsistemas

  + [Sistema Visual]({{< ref "/pages/Sistema Visual" >}})

## Puntos clave

  + Se aprende motrizmente y neurológicamente

## Notas Extra

  + Sistema de recursos es muy optimizador

    + Optimiza lo que es más estimulado y se especializa en estos

    + Por esto las personas ciegas terminan desarrollando un mejor oido por ejemplo

![Sistema nervioso central completo](/assets/bigmap400_1664756242226_0.jpg)

![Sistema nervioso central completo (Versión PDF)](/assets/thehighestofthemountains_brain_map_rev117_image_1664756047191_0.pdf)

![El esquema del cerebro.pdf](/assets/thehighestofthemountains_brain_quotes_rev27_image_1664756186695_0.pdf)

[Fuente de los diagramas y referencias: The highest of the mountains](https://thehighestofthemountains.com/Index%20of%20PDF.html)
