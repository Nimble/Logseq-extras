---
title: Modelos de disminución de error
tags:
categories:
date: 2022-09-04
lastMod: 2022-09-04
---
Dominio

  + Dominio-Muestra

    + Se  utiliza para conductas palpables (como agresividad)

      + Listado de constructos  y conceptos más representativos

        + Se abstraen las caracteristicas y manifestaciones sin una categorización palpable sobre estas - busca que no se noten los temas subyacentes y sobre lo que tratan los reactivos

  + Modelo Factorial Dominio-Muestra

    + Comprueba teorías

    + Relacionado con pruebas de validez

    + Es "selectivo" y "elitista"

      + Requiere cumplir con bastantes requisitos

Modelo de Pruebas Paralelas

  + Excelente para medir y hacer pruebas de inteligencia

  + Se mide un mismo grupo en 2 ocasiones distintas

    + Todos los reactivos son ordenados por dificultad

      + Esto da confianza al sujeto

      + Evita errores

      + La persona tiene más atención

    + Varias versiones con mismo número de reactivos y nivel de dificultad
