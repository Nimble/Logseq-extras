---
title: Respuestas
tags:
categories:
date: 2022-08-29
lastMod: 2022-08-30
---
Tangibles - Conocimientos y conductas (es certero o una certeza)

Mediantamente tangible: Aplicaciones y actividades practicas - entendimiento y conceptos

Intangibles: Apreciaciones, actitudes, intereses, juicios y opiniones. Son de medición y entendimiento o determinación más que una forma de calificación (no hay respuestas incorrectas)
