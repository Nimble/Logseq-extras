---
title: Psicología social de los grupos
tags:
categories:
date: 2022-08-28
lastMod: 2022-10-02
---
Temas centrales:

  + [Influencia Social]({{< ref "/pages/Influencia Social" >}})

  + [Psicología social de los grupos]({{< ref "/pages/Psicología social de los grupos" >}})

  + Representaciones sociales

#Porcentajes y Diseño del curso


  + Clase se basa en las lecturas

  + Clase discusión - Sesión lectura

  + Clase de Actividad

  + Asistencia cuenta (?)

  + Reportes de lectura al final de cada modulo.......................................10% Unitario............30% Total

    + Textos integrados con sus nociones más importantes mediante ejemplos

    + Se integra teoría y se trata de aplicar

      + Ejemplo: Explicar mediante la teoría del modulo lo que se ve en el metro o x suceso

  + Examenes sencillos de memoria y opción multiple.............................10% Unitario............30% Total

    + Basado en lecturas

    + Duran 30 minutos

    + Se contestan en clase

  + Practicas, actividades, tareas, clases del viernes, "cosas pequeñas"...................................40%

    + Analisis de contenido en grupo

    + Analisis de grupos en equipo

## Nociones clave

  + Psicología social se centra en el individuo y la sociedad

    + #nota-propia Estudio del alma social o compartida (?) [Geist]({{< ref "/pages/Geist" >}})

    + Las ciencias sociales son las ciencias del espíritu

      + Un ejemplo de diferentes espíritus sociales sería el de una zona urbanizada completamente, un barrio y un pueblo, son 3 [volkengeist]({{< ref "/pages/volkengeist" >}}) distintos.

  + ### #nota-propia Es la [Influencia Social]({{< ref "/pages/Influencia Social" >}}) un constructo lógico-operacional ??

  + Reclamo Social es muy importante y **razón** o **aversión** de la acertividad #nota-propia el camino fácil de menos resistencia

  + La mera prescencia y el hablar o expresar públicamente las ideas propias genera una huella e impacto social. Se expone la persona y la situa en un punto o **rol** dentro del grupo

  + Se hacen apreciaciones externas y etiquetaciones (error de atribución) por lo general en vez de una interpretación ambiental (tendencia)

  + Personalidad

    + Es situacional y es referente a rasgos individuales
