---
title: Objetividad
tags:
categories:
date: 2022-09-27
lastMod: 2022-09-27
---
No es una representación de lo real o verdadero sino del consenso comprobable y reproducible
