---
title: subitación
tags:
categories:
date: 2022-08-31
lastMod: 2022-08-31
---
"Contar" cantidades pequeñas sin hacer el proceso cognitivo de enumerar.

No existe un conteo.

Antecede otros procesos más abstractos como las habilidades lógicas y matemáticas.

  + Es una función básica
