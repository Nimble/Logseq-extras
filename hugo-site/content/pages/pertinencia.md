---
title: pertinencia
tags:
categories:
date: 2022-09-04
lastMod: 2022-09-12
---
Relación o causalidad directa relevante al organismo

  + Como sonidos a un organismo sin oido es completamente **impertinente**

Factor delimitante sobre lo que puede ser o no condicionado

Depende de muchos otros factores [biología]({{< ref "/pages/biología" >}})(icos) y mente(ales)

La pertinencia conductual es relevante al repertorio conductual del organismo

  + Deriva instintiva depende de la historia evolutiva y la pertinencia

    + Son respuestas naturales relacionadas y que compiten con el [moldeamiento de conducta]({{< ref "/pages/moldeamiento de conducta" >}})(entrenamiento condicional)

    + Caracteristicas naturales e instintivas

**RELEVANCIA**
