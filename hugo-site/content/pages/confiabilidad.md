---
title: confiabilidad
tags:
categories:
date: 2022-09-04
lastMod: 2022-09-25
---
Se refiere a la consistencia de la medida

Sirve para sacar el error estandar de medición - **consistencia o confiabilidad interna**

  + Busca correlación entre diferentes mediciones

Implica la ausencia de errores de medida

**Confiabilidad temporal**

  + Para estudios longitudinales

  + Para comparación entre diferentes tiempos o epocas

[correlación]({{< ref "/pages/correlación" >}})

Entre más grande sea la correlación **promedio** de una prueba mayor será su confiabilidad

  + Depende de el coeficiente de reactivo con todas las pruebas o reactivos del dominio

Sin embargo todas las puntuaciones son falibles

  + Siempre existe algún grado de [error de medición]({{< ref "/pages/error de medición" >}})

  + Es muy extraño tener mediciones perfectas

    + Es más probable que si las haya exista alguna falla en la medición

    + Puntaje obtenido pretende anticipar el error

    + Puntaje error +/- puntaje real = puntaje total

#tips recomendaciones

  + Correlación mínima debe ser .20

# Modelos de aumento de confiabilidad

  + Se basan en correlacionar la distancia o similitud de una medición a otra

  + ## Modelo Test-ReTest

  + Mismo instrumento a mismo grupo en diferentes ocasiones

    + Debe haber un minimo de 4 semanas entre aplicaciones

  + La desvenaja es el aprendizaje del sujeto sobre las pruebas y expectativas

  + Se usa con los modelos y formulas de Rho Y Pearson
