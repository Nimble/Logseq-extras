---
title: Redes semánticas
tags:
categories:
date: 2022-08-29
lastMod: 2022-09-04
---
Son técnicamente grafos (como lo usado en este mismo programa)

Glosario

  + Valor J - Es el tamaño de la red - Cantidad de valores

  + Valor M - Peso semántico - Que tan importante ? - Es un valor númerico (literalmente con en grafos)

  + Conjunto SAM

    + Definidor más significativo

    + Son definidores del **núcleo de red**

      + Es decir donde se centra o la mayoría de las definiciones

      + Se cortan los definidores cuando la gráfica comienza a hacerse asintotica - Y se descartan

  + Distancia Semántica Cuantitativa

    + Delimita la cercanía o similitud entre palabras definidoras con la principal, ya sea para un grupo o para un individuo

    + Se considera al valor o **concepto** más alto como 100%

    + Se hace una serie de porcentajes de los otros **conceptos definidores**

  + Índice de consenso grupal

    + Porcentaje total de acuerdos entre grupos

    + Diferencias entre pesos (para personas y grupos)

  + Carga Afectiva C.A.

    + Clasificación evaluativa de **definidoras**

    + Mide la asociación entre **definidoras**

    + $$Chi²$$

      + Se calculan definidoras de hombre y mujeres (se hace una tabla)

      + |Datos ejemplo / No ecuaciones|Masculino|Femenino|Total Marginal Columna|
|--|--|--|--|
|Definidoras Externas|20|10|30|
|Definidoras Internas|15|28|43|
|Total Marginal Renglon|35|38|Total Final = 73|

      + $$X²=\sum \frac{\left(f_o-f_e\right)^2}{f_e}\:$$

        + **Fe** = Frecuencias Esperadas

          + Se multiplica el **renglon** y **columna** de totales marginales y se divide sobre el total final (ver tabla)

            + Y quedará tambien una tabla identica a la del ejemplo 2x2 pero con los valores "esperados"

            + $$f_e=\frac{TM_r\cdot TM_c}{T_f}$$

            + **TMr** = Total Marginal Renglon

            + **TMc** = Total Marginal Columna

            + **Tf** = Total Final

            + #cuestion-propia No da esto una proporción siempre lo cual a su vez puede generar un valor estimado "falso"

        + **Fo** = Frecuencias Obtenidas

  + Grados de libertad

    + Cantidad o grado de error

    + Es una estimación estadistica que esta ya delimitada

    + $$gL=\left(c-1\right)\left(r-1\right)$$

      + c = Numero de columnas

      + r = Número de renglones

      + Este a su vez marca un valor de estimación u proporción (quizá por eso esta diseñado de esta manera) - Hay una tabla para ello en algún lugar

        + #cuestion-propia Entonces Chi² esta basada en interrelaciones propias?

        + 

  + 
