---
title: Aprendizaje y Comportamiento Adaptable 2
tags:
categories:
date: 2022-08-29
lastMod: 2022-09-26
---
#Porcentajes


  + Videos cortos (o viñetas) - 5 por unidad - Presentación de recursos por equipo

    + Explicar el porque (del recurso) y como se realizo

  + Examenes de unidades (de opción múltiple)

  + Libro/#recursos Aprendizaje y Conducta Michael Dom (?)

**ENTRENAR NO ES [Psicología]({{< ref "/pages/Psicología" >}})**

**Ambos**; condicionamiento clásico [condicionamiento operante]({{< ref "/pages/condicionamiento operante" >}}) utilizan aprendizaje asociativo. Con la diferencia de que el [condicionamiento operante]({{< ref "/pages/condicionamiento operante" >}}) usa tambien lógica proposicional, usando matematicas lo cual genera ecuaciones y sistemas.

  + La conducta se ***mide*** en unidades medibles y significativas

  + **Frecuencia de la conducta:** Es la medida de probabilidad de la conducta

Los metodos conductistas están basados en la corriente empírica

  + Asociación es el mecanismo básico

  + Iniciada por Aristoteles

    + Principios de [contigüidad]({{< ref "/pages/contigüidad" >}})

    + Similitud - asociación de dos cosas que son parecidas

    + Contraste - relación de opuestos antagonicos

    + [contigüidad]({{< ref "/pages/contigüidad" >}}) - cercania en el "tiempo"

  + Inspirados por Darwin

  + [contingencia]({{< ref "/pages/contingencia" >}}): Relación tipo causa-efecto - Es la principal idea para el condicionamiento operante

El condicionamiento esta basado en la manipulación y moldeamiento mental y conductual de organismos

  + #nota-propia El moldeamiento conductual (estimulos-respuesta) funciona con animales porque utiliza un nivel de leguaje "mental" estandarizado entre los seres vivos, es uno computacional y proposicional operante. Es justo por esa razón que se puede entablar una "conversación" un dialogo en el que se pasa información de indole conductual. Y más importante aún hay entendimiento. Esta es la verdadera razón por la cual o más bien la verdadera manera en la que se moldea este comportamiento, mediante la comunicación a un nivel bajo no verbal. Es tambien en cierto sentido una configuración conductual y social, en la cual la misma especie y qualia del individuo afecta. Es todo sobre comunicación y lenguaje - Sirve para programar porque es un lenguaje que fuerza y usa un bajo nivel de entendimiento u abstracció (nula). Es por esta razón tambien un lenguaje compartido entre todos los seres vivos y quiza incluso hasta en no vivos como los sistemas computacionals y redes neuronales - El punto central y el verbo de este lenguaje es el  **estímulos**

#nota-propia Sobre la clase anterior de ACA. El profesor tenía razón, es mejor verlo de una manera abstracta sobre los elementos que defien el condicionamiento en vez de estructuras categorizadas metodologicamente como clásico/operante

La **diferencia** entre el [condicionamiento instrumental]({{< ref "/pages/condicionamiento instrumental" >}}) y el [condicionamiento operante]({{< ref "/pages/condicionamiento operante" >}}) es que en el **primero** el sujeto se maneja de una forma **activa** y en el **segundo** este reacciona a estimulos del entorno (**pasivamente**)

  + [condicionamiento instrumental]({{< ref "/pages/condicionamiento instrumental" >}}) - Sujeto Activo

  + [condicionamiento operante]({{< ref "/pages/condicionamiento operante" >}}) - Sujeto Pasivo

El aprendizaje hace uso del _**[insight]({{< ref "/pages/insight" >}})**_ y el **ensayo y error**

#nota-propia Skinner y todo el conductismo como todo lo gringo es una puta ideación y despues ideosincracia para ganar más dinero y joder todo. Evidencias: Las cajas de Skinner patentadas, no es sobre hacer avances de conocimiento y nisiquiera cientificos, es para estampar constructos psicológicos y memes, una doctrina y ganar dinero haciendolo. Como todo lo que hacen los gringos. Breland (1961) - Agencia en Hollywood de entrenamiento animal, tambien condicionan animales y se hacen ricos lacandoles el cerebro y quitandoles su independencia salvaje, lo cual es la domesticación y la muerte natural.

Diseños del [Método experimental]({{< ref "/pages/Método experimental" >}}) en Conductismo

  + Existe cierta [Ética Experimental]({{< ref "/pages/Ética Experimental" >}})

  + estímulos pueden generar pensamientos recurrentes

  + [homeostasis]({{< ref "/pages/homeostasis" >}}) es críticamente importante

  + Es muy costoso computacional y cognitivomente mantener una atención constante

El éxito del [moldeamiento de conducta]({{< ref "/pages/moldeamiento de conducta" >}}) depende de:

  + Definir claramente la conducta objetivo

  + Aproximaciones sucesivas - pasos de entrenamiento - no se refuerza el paso anterior

  + Se tiene que considerar el tiempo y pausas en la aplicación del procedimiento

  + Respuestas del moldeamiento dependen de la variabilidad inherente de la conducta

  + Es afectado por la [contigüidad]({{< ref "/pages/contigüidad" >}}) [contingencia]({{< ref "/pages/contingencia" >}}) y [pertinencia]({{< ref "/pages/pertinencia" >}})

    + La efectividad del [reforzamiento]({{< ref "/pages/reforzamiento" >}}) afecta el comportamiento

    + 

[adicción]({{< ref "/pages/adicción" >}})


