---
title: Trayectoria de Vida o Desarrollo
tags:
categories:
date: 2022-08-29
lastMod: 2022-09-27
---
#recursos Current biology site for research.

#autores Annette Karmiloff-Smith

Se estudia principalmente la trayectoria o el desarrollo

Hace uso de [estadística]({{< ref "/pages/estadística" >}})

  + Para esto se hacen [Medición y evaluación psicológica]({{< ref "/pages/Medición y evaluación psicológica" >}}) con [Objetividad]({{< ref "/pages/Objetividad" >}})

    + Se necesitan multiples mediciones para comprender la **trayectoría** que lleva la persona o grupo y su desarrollo

  + Para determinar lo común o normativo (normal) y lo típico

  + Existen influencias [normativas]({{< ref "/pages/normativas" >}}) y no [normativas]({{< ref "/pages/normativas" >}})

    + Restriciones biológicas y ambientales - contexto - [normativas]({{< ref "/pages/normativas" >}})

      + Edad

      + Generación historíca

    + Diferencias individuales

    + Herencia

    + Medio ambiente

    + Maduración - no dijo y quien sabe

Proceso de estudio de desarrolo: **Describir → Predecir → Intervenir → Explicar**

Conceptos #clave de la materia y el desarrollo:

  + [subitación]({{< ref "/pages/subitación" >}})

  + Proceso de diagnóstico consiste en encontrar deficits en los procesos más básicos y extrapolar y explorar esta raíz. Sintetización, focalización y atomización.

  + Todo proceso cognitivo depende de las restricciones biológicas y ambientales

    + Ejemplo: [neuronas]({{< ref "/pages/neuronas" >}}) binoculares

  + Periodos críticos o sensibles

    + Lapsos precisos en los cuales se deben desarrollar ciertas caracteristicas

      + Como el lenguaje en los primero años de vida (allegedly), para que se tenga un dominio completo del mismo (nativo)

        + El cerebro se tiene que acostumbrar al sonido y formas motrices del lenguaje

          + Mediante la plasticidad cerebral

  + **Epigenética** - ambiente detona componentes innatos

  + El objeto del estudio es el **cambio** y el **desarrollo**

Perspectivas o dimensiones del desarrollo

  + Físico

  + Cognitivo

  + Social-emocional

Las [etapas de desarrollo]({{< ref "/pages/etapas de desarrollo" >}})

Enfoque del [desarrollo de vida de Baltes]({{< ref "/pages/desarrollo de vida de Baltes" >}})

Vertientes principales del desarrollo

  + John Locke - Mecanismo - Empirismo ↣ La mente es solo experiencias

  + Jean Jacques Russeau - Organismo - Innatismo ↣ Existen saberes fuera de la experiencia (de lo que conocemos) - #nota-propia Es un propio paradojico no?

## Métodos

  + ## **Diseño de caso único**
 - Casos Clínicos y aplicados solo a una persona

  + Estudio de diseños muestrales - Para grupos

    + Es un poco tramposo pues existe un compromiso tácito pues la [estadística]({{< ref "/pages/estadística" >}}) elimina erroes al promediar ↣ lo cual genera ↣ Distribuciones Normales y anula personas o casos fuera de esos rangos

      + En el entender estos [error de medición]({{< ref "/pages/error de medición" >}})(de comparación), su origen, y porque su incidencia ocurre de alguna manera se busca conocer y controlar este error y eliminarlo

        + El **error individual** se elimina conociendo el **proceso** completo del fénomeno

        + El **error muestral** se elimina [estadística]({{< ref "/pages/estadística" >}})mente por el promedio

#tips Es más fácil hacer un diseño transversal pues se toman muestras en un solo momento, en vez del longitudinal que se hace una medición repetida trayectorial sobre la misma población. Estudios longitudinales son usualmente más caros. Diesños mixtos longitudinales y transversales son los mejores.

[genética]({{< ref "/pages/genética" >}})
