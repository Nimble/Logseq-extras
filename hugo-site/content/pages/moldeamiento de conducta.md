---
title: moldeamiento de conducta
tags:
categories:
date: 2022-09-04
lastMod: 2022-09-27
---
[aversiva]({{< ref "/pages/aversiva" >}}) - [apetitiva]({{< ref "/pages/apetitiva" >}}) // castigo - recompensa

Se basa completamente en el manejo de probabilidades, es decir alterar que tan probable o no es una conducta

  + conducta ↣ consecuencia ↣ [contingencia]({{< ref "/pages/contingencia" >}}) [apetitiva]({{< ref "/pages/apetitiva" >}}) o [aversiva]({{< ref "/pages/aversiva" >}})

    + La contingencia puede ser un **Reforzador** o un **Castigo**

Moldeamiento programacional usa una matriz:

  + |Métodos|Positivo|Negativo|
|--|--|--|
| [reforzamiento]({{< ref "/pages/reforzamiento" >}}) |R+|R-|
| [castigo]({{< ref "/pages/castigo" >}}) |C+|C-|

Entrenamiento por omision

  + Entrenamiento diferencial de otras conductas

    + Respuesta elimina quita o previene un estimulo apetitivo

    + Castigo (castiga) la respuesta positiva

Se debe evitar la negación en las proposiciones

Referencias clave

  + Variabilidad: Repertorio conductual amplio

  + Estereotipia: Forma única y general de comportamiento

Hacer abstracción de la conducta deseada y toda conducta que puede ser condicionable (progamable)

  + Implica que el condicionamiento puede aumentar la variabilidad y esteropización dependiendo del diseño del moldeamiento

Variaciones pequeñas en [reforzamiento]({{< ref "/pages/reforzamiento" >}}) puede causar gran impacto en el moldeamiento

  + Esto se debe a la representación cognitiva que tiene el organismo sobre distintos estimulos, en relación a su [pertinencia]({{< ref "/pages/pertinencia" >}}) y las comparaciones cognitivos

    + Compara los ejes mencionados después con eventos pasados [memoria]({{< ref "/pages/memoria" >}})

  + Ejes de cantidad y calidad afectan

Relación entre conducta y respuesta puede ser probabilistica (que tan probable es x acción que suceda/elección)

No todas las acciones tienen consecuencias conductuales (reforzadores/castigos)

## Métodos alternos de condicionamiento

  + ## Reforzadores secundarios o condicionados


  + Método de marcado

    + Hacer distinguir (marcar/highlight) la conducta objetivo

    + Se le dan estimulos extra de muestra NO asociados con el Estimulo Incondicionado

  + Indefensión aprendida

    + Hace más lento el aprendizaje superior

    + Catch 22 - Sin importar que respuesta o elección sea tomada y realizada la consecuencia será la misma - Paradox - Paradojico

    + Derivado de que la conducta/respuesta no vale nada en relación a refuerzos aversivos

    + Sujetos entran en inacción (se rinden ante su situación)

      + Una raiz de la manipulación social y el nihilismo

  + Programas concurrentes

    + Hay al menos dos reforzadores u opciones presentes al organismo en el mismo espacio de tiempo

    + No son solo dos elecciones sino dos vertientes de reforzadores y estimulos que un organismo puede elegir

## Ley de igualación

  + La tasa relativa de respuesta es igual a la tasa relativa de refuerzo

  + Relacionado a Rescorla-Wagner (?)

  + $$\frac{R_I}{R_I+R_D}=\frac{C_I}{C_I+C_D}$$
