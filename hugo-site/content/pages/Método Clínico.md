---
title: Método Clínico
tags:
categories:
date: 2022-08-29
lastMod: 2022-09-23
---
Set de reglas de evaluación que están validadas por distintos marcos teoricos

  + Conductas

  + Procesos cognitivos

  + Personalidad

  + Procesos biológicos

  + Completa evaluación física y mental

  + Factores socioculturales

Evaluación -> Diagnostico -> Intervención

  + Diagnóstico

    + Identificar

    + Rastreo y entendimiento de la evaluación que lleva a una proposición o hipotesis

  + Intervención

    + Prevención - Como promoción de la salud

    + Interacción con el sujeto

    + Tratamiento

El proceso Clínico

  + #.ol-right-paren Fenomeno en observación

  + Se explora y se plantea el problema

  + Se describe y se focaliza (proceso diagnóstico)

  + Deliveración: Se transforma en una serie de preguntas de investigación

  + Proceso evaluativo: Se plantea que se quiere responder

    + Se plantea un diseño de investigación basandose en el proceso evaluativo (evaluación y tests)

  + Se consideran todas las variables

  + Se ponen todas las hipotesis en prueba

    + Se corrobora o se confirma la hipotesis

  + Se ejecuta el proceso de intervención y prevención

    + Se analiza e interpretan los datos
En planteamiento del problema nos encontramos con el problema subyacente de la persona.
Indagacion: Tema abstracto a explorar dentro de las entrevistas interacciones etc.

procesos biospicosociales

  + Procesos cognitivos

  + Personalidad

  + Rasgos caracteristicos (caracterológicos)

  + Interacciones y contextos - El entorno

Salud Mental

  + Busca el bienestar del sujeto que le permita _funcionar_ dentro de su entorno - Considera absolutamente todos los procesos biospicosociales y es **diferente y variado dependiendo del sujeto**

Linea del tiempo con la [historia de la clínica]({{< ref "/pages/historia de la clínica" >}}) en occidente

#cuestion-propia ¿Cómo será la doctrina clínica en el este? (Asia, Medio Oriente y America)

#cuestion-propia El lenguaje y el psicoanalisis y derivados

  + El psicoanalisis de Freud fue la vertiente más popular o aceptada porque es el que hacía uso de un lenguaje más primitivo (siendo este el del líbido/sexo) para el entendimiento de la psique, en otras palabras y conscisamente es algo compartido por todo lo que entendemos como especie humana, por eso mismo este eje o campo común es lo que puede crear o ser un medio de comunicación entendible en terminos generales. Su popularidad depende y se relaciona a ese factor, que es una descomosición o explicación del libido y lo combierte o usa simbolos sexuales para el entendimento y eso es algo que toda persona entiende. Sin embargo otras vertientes analiticas como la de Jung no fue tan popular por una gran cantidad de razones hasta politicas, pero tambien a un nivel fundamental todas las personas pueden entender a Freud cuando habla de simbolos sexuales pero no a Jung cuando habla de simbolos espirituales o animosos, pues estos son constructos y entendimientos más complejos y mucho menos primitivos que el sexo en un nivel abstracto y porque la interpretación varía enormemente dependiendo del contexto de la persona y de sus experiencias.

Los 3 métodos

  + Todos buscan validez interna y congruencia

  + [Método Experimental]({{< ref "/pages/Método Experimental" >}})

  + [Método Epidemiológico]({{< ref "/pages/Método Epidemiológico" >}})

  + [Método Clínico]({{< ref "/pages/Método Clínico" >}})

    + Problema y motivo de consulta (malestar)→ Es el objetivo de atención - Necesidad de Cambio

    + Explora - investiga - trata

    + Información - exploración (usando instrumentos) - [observación]({{< ref "/pages/observación" >}})

      + Entrevistas

      + Escalas/tests

      + Cuestionarios

      + Historia clínica

        + Signos y sintomas

          + Problematica y afectaciones

            + Familiares

            + Sociales

            + Pareja

            + Escolar

            + Laboral

            + Etcetera

        + ¿Cúal es la situación desencadenante y si existe alguna predisposición?

        + Conocer la trayectoria o curso del malestar (su desarrollo)

        + Antecedentes familiares

          + Social, cultural, economico, roles, etc.

          + Dinámica familiar

          + Historia personal

          + Consumo de sustancias → hábito

          + Antecedentes escolares y de contexto

          + Intereses, etc.

        + Estado mental

          + Percepciones espacio-temporales y emotivas

          + Forma y contenido del **discurso**

          + Analisis de vestimenta

    + Impresión diagnostica

      + No quedarse nunca únicamente con la clasificación instrumental de los manuales (diagnostico formal)

        + Son solo indicadores y cierra el discurso y experiencia de la persona

    + Propuesta de intervención

      + Implica enfoques teoricos

        + como conductivo conductual, psicoanalisis etc. - Paradigmas al malestar

    + Resultados y evaluación de las medidas tomadas

      + El foco total del método clínico es la observación y posteriormente la deducción en el proces indagatorio

    + Diagnostico informal

      + Explicación usando enfoques teoricos y con los procesos cognitivos del psicologo

    + Diagnostico formal

      + Resultados exclusivos de los tests y otros instrumentos (estandarizados) utilizados

    + Reporte psicológico

      + Previo al diagnostico

      + Informe de todo lo conocido sobre el caso

        + Instrumentos aplicados

        + Historia clínica

      + Propone supuestos e hipotesis

      + Propone sugerencias e interconsultas con otros campos

      + Se da a saber y conocer

        + Formula demandas

        + **Paciente**: Esto es lo que quiero - no por esto es que estoy aquí

**Signo** - objetivo - visual/visible - cuantitativo

**Sintoma** - percepciones sobre el malestar - lo que dice la persona
