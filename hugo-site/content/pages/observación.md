---
title: observación
tags:
categories:
date: 2022-09-23
lastMod: 2022-09-27
---
## Tiene una forma cientifica (Experimental)

  + Sistematico - implica que existe un objeto de estudio

  + Es ordenado y tiene pasos como un algoritmo

    + Se usan formas (constructos) conductuales (paradigma/enfoque)

    + Determinar la situación

      + ¿Qué se quiere observar?

        + Indagar frecuencia y tiempos

    + Definir elemento o muestra de la conducta

      + Definición operacional de la conducta

      + En otras palabras que es "x" acción - ¿Cómo es definida?

    + Precisar las condiciones de la observación

      + Bajo que situaciones se observa

        + Incognito

        + Controlado

    + Crear documentos de observación

      + **Sistematicos**

        + Lista de cotejo

        + Registro anecdotico

        + Estudio de campo: Detalla minuciosamente todo comportamiento observable

        + Escalas númericas (Frecuencias de conducta)

          + Uso de categorización (siempre-aveces-nunca)

        + Escalas gráficas

          + Se clasifica en subdimensiones (cuantos/quantum)

          + Espectro de las conductas y como esta transita de positivo a negativo

        + Escalas descriptivas

          + Correlacional - ¿Cuando ocurre más la conducta?

        + Unidades de medida

          + Ocurrencia: si ocurre o no si/no


          + Frecuencia: que tanto ocurre

          + Latencia: tiempo entre estimulo y respuesta

          + Duración: cuanto dura la respuesta

          + Intensidad: que tan extrema es la respuesta

      + **Registros NO sistematizados**

        + Historias de vida

        + Autobiografia

        + Entrar en profundidad

        + Diarios

        + Cuadro de notas

        + Registros mécanicos

        + Video, audio, fotografía

  + Es controlado - con variables controladas

## Tambien puede usar una forma **no** cientifica

  + Solo se observa el comportamiento y la conducta manifiesta

  + Debe determinar la forma objeto-situación-caso

    + Objetivos

    + Forma de registro:

      + Observa y registra

      + Analiza e interpreta

    + Se elabora un informe final con todo lo observado

      + Si hay observaciones desde distintos observadores se divide y junta o compara perspectivas

## **Diseño de caso único**
  + Método para establecer relaciones causales del individuo en su contexto - Pretende conocer la situación en su conjunto (holisticamente)

  + Accionar clínico u objeto de estudio (unidad de análisis)

    + Individuo

    + Intervención

    + Problemática

  + **Estudio de casos no controlados** (sin control de las variables intervenientes)

  + **Diseños experimentales de caso único**

    + Control de las variables participantes del estudio - **relaciones de orden causal**

    + Control de la intervención

    + Evaluación del rendimiento (tiempo-condiciones)

    + Busqueda de configuraciones *intracaso* (predicciones)

  + **Diseños observacionales de caso único**

    + Cuasiexperimentales: Observa una manipulación directa de la Variable Independiente

    + Ejemplos:

      + Intervención terapeutica

      + Estudios de caso de evaluación diagnostica

  + Presentación de caso

    + Generalización

      + Heterogeneidad de los sujetos

      + Estandarización de la tx

      + Integridad del tx

      + Impacto multidimensional del tx

      + Significancia clínica

    + Evaluación continua - Evaluación de línea base (descriptiva, predictiva) - variabilidad - estabilidad - redimiento

    + Estrategias de investigación

      + Tipos de pregunta de investigación

      + Grado de control sobre el comportamiento _real_

      + Enfoque temporal - Como se abordará la observacion

        + Historico - que paso y como terminó donde esta

        + Contemporaneo - el aquí y el ahora de su situación
