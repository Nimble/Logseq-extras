---
title: Reactivos
tags:
categories:
date: 2022-08-29
lastMod: 2022-09-26
---
# Elaboración de reactivos

Al usar distintos tipos de reactivo se cotra la monotonía y se regana la atención

### Tipos

  + Verdadero/Falso

    + Evalua memoria a corto plazo

  + Completamiento o respuesta corta

    + Mide memoria

    + Necesita buena redacción

    + Forma expectativas y puede predisponer una respuesta o idea (adivinanzas)

  + Jerarquización

    + Debe tener solo un orden o secuencia (algoritmo)

    + Puede medir memoria o comprensión

  + Apareamiento o asociación

    + Relacionar columnas debe de ser del mismo **tema** por la forma en la que aprendemos

  + Opción multiple

    + Más difíciles de elaborar

    + Mide conocimiento - No memoria

    + Preguntas deben ser simples y sencillas

      + Opciones no deben seguir patrones

  + Pregunta abierta o ensayo

  + Escalares

    + Instrumentos de escalas

    + Son cuantitativos

    + Estan graduados

Escala Likert Pictorica - cuadros de distintos tamaños que implican incidencia o rostros ![Escala Likert Pictórica](https://c.neevacdn.net/image/fetch/s--dgwal6en--/https%3a//crehana-blog.imgix.net/media/filer_public/de/ff/deffb07f-fb5c-4227-928b-d5fe94b38dab/likert-type-scale-responses.png%3fauto%3dformat%26q%3d50?savepath=likert-type-scale-responses.png)

### Conceptos #tips

  + Conceptualización

    + Tener completamente claro semanticamente el constructo - entenderlo y saber que es

    + Significado

    + Muestreo de contenido

  + Tipo de lenguaje

    + Varia dependiendo la edad

      + Generalidad del lenguaje

      + ¿Cómo formular los reactivos?

      + ¿Qué tipo de palabras usar?

  + Respuesta automática-aquiescencia (response set)

    + Debe evitarse para que las personas contesten honestamente

    + Usar estimulos (como completación) y variar reactivos y redacción

  + Deseabilidad social

    + Evitar opciones "siempre y nunca" - alteran la _deseabilidad_ del sujeto

    + Tendecia de las personas a contestar de la manera correcta o esperada

  + Debe ser la misma cantidad de reactivos por dimensión a medir distribuidos en orden aleatorio

  + Piloteo

    + Prueba del instrumento en grupos semejantes al deseado

      + Sirve para ver como se comportan los reactivos

      + Similar a un ensayo

      + **Dar especial atención a sugerencias y observaciones**

      + Mide atractividad de las opciones de respuesta

      + Considerar la varianza en las respuestas

        + Si alguna se repite mucho es mejor eliminarla - es decir concentración de varianza en solo una opción

        + _Purificación_ de reactivos: arreglarlos/mejorarlos

    + Personas por reactivo (muy bien)10 > (óptimo/decente)7 > (terrible)5

  + Análisis de reactivos

    + Distribución normal o sesgada

      + Depende completamente del constructo que se quiera medir

    + Frecuencias y sesgo

      + ¿Qué tanto se repite algo?

      + Se reporta la incidencia/prevalencia solo si es superior al 50%

        + Lo cual muestra que es una respuesta muy deseable o atractiva y debe ser eliminada

    + Direccionalidad (crosstabs [análisis factorial]({{< ref "/pages/análisis factorial" >}}) )

      + Normal -.5 +.5

      + Sesgada

    + Se eliminan reactivos en el proceso de **discriminación** t-student

    + validez <(depende de)> [análisis factorial]({{< ref "/pages/análisis factorial" >}})

      + Obtención de normas

## Distractores

  + Siempre arriba u abajo de la respuesta correcta (contiguo)

  + Ratio es 5 opciones correctas y 2 falsas = 7 total

  + No debe tener duplicados

## Errores o fallas en reactivos

  + ### Chic@s faciles

  + Reactivo con 2 o más factores con la misma carga

  + Significa que no sirve para medir

  + #tips se eliminan los que pesen menos de .40 o .30

Los reactivos tambien se conocen como **factores** cuando estan forma matricial (tabla comparativa [correlación]({{< ref "/pages/correlación" >}}))

  + Los factores son una agrupación de reactivos (?)

# Método de componentes principales

Primer factor es el que describe mejor al sujeto

Reactivos sin pertenencia a algún facor son eliminados

Solo los factores con más de 3 reactivos son considerados

  + En otras palabras el constructo debe de ser medido por más de 3 preguntas

  + Eliminación por columna (factor) < 3

  + Eliminación por renglon (reactvo) > 2

**Cada factor tiene un peso propio o Eigen Value**

  + El primer factor es el que más explica la muestra

    + Es el que tiene más [varianza]({{< ref "/pages/varianza" >}})

    + Es el que tiene mayor agrupación semantica de reactivos

    + Define la muestra

      + Todo es relacional y depende del primer reactivo

      + Es el más caracteristico del constructo
