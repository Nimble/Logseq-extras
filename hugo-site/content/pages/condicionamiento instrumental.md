---
title: condicionamiento instrumental
tags:
categories:
date: 2022-08-30
lastMod: 2022-09-04
---
Se le llama **instrumental** porque estudia o considera la conducta un instrumento

  + El resultado del entorno lleva a la puntuación

Implica una meta y proceso específico

El entrenamiento por moldeamiento es secuencial

  + Se deben enseñar los comportamientos más sencillos hasta los más complejos

  + Se tiene que analizar la conducta objetiva final

    + Para delimitar las maneras en las que se puede llegar a esta sistematicamente

  + Las conductas de pasos anteriores no son reforzadas sino las nuevas o presentes

#nota-propia es literalmente una programación cerebral/mental y una abstracción del lenguaje

Organiza y jerarquiza conductas ya conocidas por el organismo, las cuales orienta a cierto fin o forma nuevas

  + Tambien altera conductas ya conocidas
