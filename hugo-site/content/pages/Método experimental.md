---
title: Método experimental
tags:
categories:
date: 2022-08-30
lastMod: 2022-09-12
---
Índoles: Exploratorio → Descriptivo → Correlacional → Experimental

Es un algoritmo

  + Justificación

  + Objetivos - Problema

  + Hipotesis H0 y H1

  + Diseño de instrumentos

    + Variables: Dependientes e independientes

  + Analisis de resultados

  + Conclusión

Dentro del conductismo y [Aprendizaje y Comportamiento Adaptable 2]({{< ref "/pages/Aprendizaje y Comportamiento Adaptable 2" >}})

  + Se debe que considerar la intensidad de la substancia

    + Puede opacar completamente completamente otros estimulos
