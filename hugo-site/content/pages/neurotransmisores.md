---
title: neurotransmisores
tags:
categories:
date: 2022-10-02
lastMod: 2022-10-02
---
Dopamina

  + Activa y es estimulante de los [Sistemas Neuronales]({{< ref "/pages/Sistemas Neuronales" >}})

  + Activa centros del **despertar**

  + Genera que se abran canales de sódio

  + Algunas drogas conocidas como **agonistas** abren canales

    + El uso de estas drogas puede llebar a una sobreliberación y abundancia de dopamina en el espacio intercerebral.

      + **Lo que esta en las vesiculas no se libera nunca**

        + Y los productores dejan de producir dopamina

    + Los receptores al experimentar tanta dopamina se "meten" debajo o dentro de la [neuronas]({{< ref "/pages/neuronas" >}}), proceso es conocido como **Infraregulación**


  + Dopamina (es biognénica) → Serotonina
 y ((633a4656-f086-415b-a8b4-e3f9809b82cb))

Serotonina
Noradrenalina