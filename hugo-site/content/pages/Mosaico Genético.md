---
title: Mosaico Genético
tags:
categories:
date: 2022-09-30
lastMod: 2022-09-30
---
Entre las causas frecuentes están la no disyunción en una división mitótica poscigótica temprana, un fenómeno de lionización, una anafase retardada y de la endoreplication.El retraso en la anafase parece ser el principal proceso por el que se produce mosaicismo en la preimplantaciónel del embrión. El mosaicismo también puede resultar de una mutación durante el desarrollo que se propaga a sólo un subconjunto de las células adultas.

Este fenómeno se transmitirá a las células descendientes, pero no a las restantes, originando las dos poblaciones de células distintas. Células afectadas

El mosaicismo puede afectar a cualquier tejido del organismo. Además, hay que resaltar que el mosaicismo no es estático, sino que puede variar el genotipo de normal a anormal, o viceversa. Existen dos tipos de mosaicismo:

Mosaicismo somático en el que coexisten células normales y anormales
