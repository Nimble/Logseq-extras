---
title: Taller de psicofisiología
tags:
categories:
date: 2022-08-28
lastMod: 2022-10-02
---
[Lecturas de Psicofisiología]({{< ref "/pages/Lecturas de Psicofisiología" >}})

## Temas principales

  + [Estructura Neuronal]({{< ref "/pages/Estructura Neuronal" >}})

  + [Sistemas Neuronales]({{< ref "/pages/Sistemas Neuronales" >}})

  + [Técnicas Neurológicas]({{< ref "/pages/Técnicas Neurológicas" >}})
