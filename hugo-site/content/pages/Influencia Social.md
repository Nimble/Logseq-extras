---
title: Influencia Social
tags:
categories:
date: 2022-09-02
lastMod: 2022-10-02
---
Las masas afectan al individuo

Los ejes son

  + Psicológicos

  + Sociales

    + actitudes y conductas

  + Folklorico/cultural

    + Esto genera normas y creencias

Zanie - Leyes sociales como _aproximación_

Bagby

  + Reacciones

  + Situaciones

Allport

  + Imitación

  + Leyes sociales

  + Vida social afecta conductas (como crimen y castigo)

Goffman

  + La interacción viene en forma de reglas o guiones sociales - #nota-propia Falsedad e imagenes en forma de imitaciones sociales

La socialización integra reglas sociales a partir de imitación implicita y explicita - aprendizaje vicario

  + Las sonrisas son símbolos falsos; se sonrie por supervivencia y aprendizaje vicario y gestalt - es una respuesta arbitraria a la alegria ((según))

[mentes sociales]({{< ref "/pages/mentes sociales" >}})

La situación puede invalidar o anular (o sobrepasar/ignorar) (*override*) la personalidad individual en ocurrencias grupales - Se crea una realidad generalizada y aceptada en grupo - Se pierde la individualidad completamente

#nota-propia La fuerza social [volkengeist]({{< ref "/pages/volkengeist" >}}) supera el espíritu del individuo - La lucha es entonces sobrepasar esa fuerza externa con el ser y certeza mismo de y mediante la individuación, autoautoria, ser uno, único, verdadero, real. El tener autoridad y autonomía propia sobre uno mismo y sus elecciones es a su vez *libertad* y **soberanía**. Todo esto previene efectos sociales como la validación, socialización, imitación, etc.

#nota-propia Es difícil oponerse y esta dificultad aumenta en grupo ¿Por qué? y en esa misma línea ¿Porqué es más sencillo ser pasivo a situaciones y la inactividad? - [El camino de la menor resistencia]({{< ref "/pages/El camino de la menor resistencia" >}})

## Conceptos Clave

  + **Todos estos conceptos influyen en la persuasión**

  + Principios sistematicos del conductismo

    + Interes, [pertinencia]({{< ref "/pages/pertinencia" >}})/importancia afectan

      + Personas cuya opinión importa más en la conducta en probabilidad se ve afectada por este peso

      + Cercanía temporal y de distancia e inmediatez

      + La cantidad tambien afecta (tamaño del grupo) - Autoatención - ¿Cúal es la relación con el grupo mayoritario?

      + Factores importantes: fuerza, cercanía, tamaño

  + **La unanimidad tiene gran impacto (Ej: todos contra uno)**

  + ### Disonancia cognitiva
    + Desconfort en la inconsistencia ¿Qué tan fuerte es el impacto de este?

    + La forma de lidiar del sujeto con esta situacipon es una racionalizacion que balancea la mente hacía las acciones que toma.

  + Malestar de Fenstinger (?): Tensión

  + Autopresentación: Being a bullshiter

    + El proceso de balanceo es lo que genera la alteración de la conducta. Es el estrés de la disonancia, el no saber como lidiar con esa otra _verdad_ y la disonancia misma lo que _rompe_ a la persona

  + Exoticidad

    + Personas distintas a las del contexto

  + Persuasión-receptor - transmisión de ideas (fuerza)/cambio ↣ Aceptacón y recepción del mensaje

  + <ins>Aceptación del mensaje depende de que tan <b>cercano</b> sea al sujeto</ins>

    + Se tiene que considerar a la audiencia receptora y todas sus características

  + ### Teoría de la reactancia

    + Al quitar el poder de elección (restringir poder de elección y limitar opciones) ↣ La persona es proclive a tomar o hacer la reacción opuesta a la de esta limitante. En corto; **Cuando algo se impone se realiza la acción opuesta (llevar la contraria)**

    + Acción opuesta a la pérdida de libertad

    + vulnerabilidad genera reactancia

  + **Motivación inconsciente**

    + estímulos no percividos afectan al receptor - Son procesos de los cuales no nos damos cuenta

  + **Recuerdo del mensaje**

    + Accesibilidad a la actitud

      + Disposición del receptor

    + Efecto de primacia - Se recuerda el primera información (será tambien lo que moldea la recepción?)

    + Efecto de recencia - Se recuerda el cierre/conclusión

    + **Efecto retardado**

      + **Expectativa de procesamiento y vinculación de información con otra en un momento futuro**

  + ### Teoría de la inoculación

    + El conocimiento previo de un tema es difícil de alterar (y el saber sobre persuasión dificulta la persuasión en el sujeto) - Asignación de crédito y control de información

  + ### [influencias]({{< ref "/pages/influencias" >}})

    + Directas o indirectas

  + ### Validación social
    + Conducta correcta a partir de comparaciónsocial (conducta social y politicamente aceptable)

    + En **relación** a grupos al mismo tiempo que los **fuerza**

    + Ejemplo: Lo más vendido ejerce presión o influencia por su reconocimiento

  + Evitamiento

    + Poner distancia

  + Impacto **negativo** siempre es temporal pues existe desensibilización

  + **Solo hacen falta 3 personas para crear un efecto de influencia social (porque?) sobre singulares e incluso grupos mayores**

  + ### Interiorización

    + Reglas o normas sociales que el individuo acopla como propias

    + Influencia social normativa implicita

    + Existe coerción

  + **Minimamente debe existir alguna motivación interna**

  + ### Idetificación

    + Conductas de [imitación]({{< ref "/pages/imitación" >}}) solo con personajes con cierto grado de [identificación](##### Simpatía
) (alter(?))

  + ### Internalización

    + Identificación de envolvimiento o integración de las caracteristicas admiradas de otro

    + Los elementos similares de otro se vuelven propios (solo si es alguien admirado o en alta estima)

  + **Efecto autokinético** → rumores - ### Complaciencia


  + **Mustafat Sheriff -** La realidad subjetiva se crea a partir de la creencia de una realidad objetiva - Percepción fisiológica por los ojos

  + ### Moscovici

    + Nosotros - **(claridad de diferencias)** - Ustedes/Otros

    + Influencia de la minoría (activa) → Que se convierte en una mayoría mediante un proceso de ## Persuasión


      + Para que esta minoría tenga efecto la consistencia de sus acciones debe ser siempre marcada - Esto define al grupo al exterior y le brinda una identidad sólida

      + La finalidad es la normalización de la idea minoritaria

        + Se requiere un consenso gradual (aceptación lenta)

      + Estos cambios a menos que se interioricen e internalicen habrán pasado por un proceso de ### Conformidad
 políticamente correcta. En otras palabras no existio convencimiento ni cambio interno verdadero.

    + Proceso de innovación - resuelto a favor de la minoría (persuasión) - será acaso un lavado de cerebro (?)

  + ### Pilares Principales del Convencimiento

    + No tener absolutamente nada que ganar - razón desquiciada

    + Ser congruentes - **ser y hacer**

    + El apendizaje mismo es cuestionado y criticado

## Persuasión
  + ### Persuasión directa

    + También conocida como **Ruta central**

    + Modificación directa de la conducta

    + Espera el entendimiento del discurso

    + Implica el uso del argumento

  + ### Persuasión indirecta

    + Tambien conocida como **Ruta periférica**

    + Constituye todo lo que hay alrededor del mensaje a excepción del mensaje mismo (el argumento)

      + Asociaciónes personales y colectivas

      + Apologías a aspectos irrelevantes

      + Todo lo que no es el argumento

      + Todo lo extra que ayuda a la atención y convencimiento

      + Generar urgencia y otras emociones

    + #### Métodos

      + Modelo **heurístico** de reglas sociales faciles - **no se piensa o se reflexiona** - ¿Cómo modo automático?

      + Más es mejor

      + Autoverificación: Pregunta final retorica

      + ##### Coherencia y simpatía

        + Compromiso y coherencia en acción

        + Forma de convencimiento o influencia que genera una **necesidad** de _pagar_ o dar de vuelta algo otorgado

        + Opera de una manera **implícita**

      + Congruencia y compromiso

        + Emociones e ideales son base fundamental para este método

        + Se cuestiona al sujeto sobre sus acciones y se le fuerza a compararlas con sus ideales

      + Puerta en la cara

        + Se tiene una expectativa de negativa

        + Se usa reciprocidad y genera una idea falsa de negociación

      + Pie en la puerta

        + Restricciones y convencimiento usados para convencer al sujeto que ya esta de alguna manera involucrado o comprometido

        + Ejemplo: Ir una milla extra como las tiendas que dan un extra u oferta al comprar algún producto

      + Bola baja

        + Información incompleta (omisión y/o desinformación)

        + No se cambia el tema o foco

        + Existe un enganche escondido

        + Utiliza presión del tiempo y otras [influencias]({{< ref "/pages/influencias" >}})

        + Se llama bola baja porque no se ve venir

      + Incluso un peñique es suficiente

        + Dar poco es mucho y valioso

        + Ejemplo: La cruz roja

      + ##### Simpatía
        + Personas reconocidas

        + Personas similares

        + Caracteristicas relatables o aspiraciones (emisor)

          + Físico

          + Similitud

          + Pausas

          + Verbalización

        + Extraccipon de conclusiones ↣ Escacez


        + Ejemplo: Personas famosas

  + ### Ruta profunda

    + Reflexiva - Hace al sujeto o grupo reflexionar (genera ### Disonancia cognitiva
 )

    + #### Métodos

      + ##### Coherencia y congruencia

        + Se tiene que explicar la conducta pues se quiere evitar una ### Disonancia cognitiva
 - Hace que el sujeto se cuestione a si mismo

  + ### Última meta o última forma de persuasión
    + El llamado lavado de cerebro (adoctrinamiento)

    + Quitar, arrebatar y modificar el sistema de creencias a algun individuo o grupo

      + Se aisla a la persona completamente

      + Carencia de una red de apoyo

      + Existe control de información

      + Control de las necesidads básicas (como la libertad de cuando ir al baño)

      + Se le quita y destruye su identidad

      + Se controlan todos los efectos a sus acciones

## Comunicación masiva

  + Hay oidos sordos

  + **Activación**: Focaliza en posibles personas interesadas

    + Ejemplo: Promociones

## Rituales

  + Son generados por [imitación]({{< ref "/pages/imitación" >}}) y el procso de (condicionamiento) validación social que llega a la socialización

  + Genera un compromiso privado y hasta [adoctrinamiento](### Última meta o última forma de persuasión
)

  + 

## Experimentos

  + Experimento de Stanford-Sinbad 1971

  + Sociedad latinoamericana de psicología - Zambardo

  + Psicología de Yucatán (sobre la reactancia)

  + Experimento Milgram - El de las cámaras y electroshock falso

    + Prueba influencias de #### Autorización
 y ((633934a7-e705-4156-baff-34caf91acafa))

#nota-propia Se venden ideas en los comerciales, es una implantación de **reconocimiento** de marca, un recuerdo y familiaridad más que el vender el producto como tal o la idea del mismo o alrededor de este
