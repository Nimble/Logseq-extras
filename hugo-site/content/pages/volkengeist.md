---
title: volkengeist
tags:
categories:
date: 2022-10-01
lastMod: 2022-10-02
---
Tipos (básicos):

||Colectivismo|Individualismo|
|--|--|--|
|Vertical|Jerarquía de grupos|Se ve únicamente por el individuo (uno por uno)|
|Horizontal|Grupos no tienen jerarquización → Se actua conforme al grupo relativo a la acción|Colaboración solo existe cuando también hay una ganancia personal|
