---
title: análisis factorial
tags:
categories:
date: 2022-09-25
lastMod: 2022-09-25
---
## Rotacion Varimax

  + Orden de las mallecillas del reloj

  + Se usa cuando el instrumento y el investigador tienen bien definidos el **constructo** a medir

  + Es contiguo, se conocen variables y asociaciones

## Rotación oblim-oblicua

  + Usado cuando no se conocen las variables

  + Reduce el 100% de la [varianza]({{< ref "/pages/varianza" >}})

  + Busca afinidad a partir de este tipo de rotaciones
