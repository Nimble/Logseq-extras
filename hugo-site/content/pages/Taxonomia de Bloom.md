---
title: Taxonomia de Bloom
tags:
categories:
date: 2022-08-29
lastMod: 2022-08-29
---
Hay dos versiones, una nueva y una vieja

Técnicas intelectuales

Objetivos de instrucción - Cual es el diseño que el curso tomara (orientación)

  + Cognositiva - Especialización de Bloom (?)

  + Afectiva - Pretende dotar al sujeto de un **ajuste** adecuado social y moralmente

  + Psicomotor

  + Cognición

    + Conocimiento - Hace referencia a la memoria. Es información que el sujeto conoce.

    + Comprensión - Entendimiento e integración de tema con otros (niveles básicos)

    + Aplicación - Se ve en practica el conocimiento de lo comprendido (pruebas)

    + Analisis - Comprensión profunda del tema. Implica juicio y reflexión sobre los temas ya comprendidos. Puede usar jerarquización de conceptos.

    + Sintesis - Abstracción base de todos los elementos de un tema a su idea más fundamental. Ejemplo: Frases de impacto y concretas que focalizen y sean un pilar informativo.

    + Evaluación - Capaz de hacer juicios sobre la idea cognitiva de un tema. Se es un experto que tiene todos los elementos para críticar el tema.
