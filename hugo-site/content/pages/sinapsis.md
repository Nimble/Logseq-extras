---
alias:
- sináptico
title: sinapsis
tags:
categories:
date: 2022-10-02
lastMod: 2022-10-03
---
Los potenciales postsinapticos (inhibidor y excitador)

  + Solo ocurren cuando se rebasa un umbral de -40μV y dura ~1ms~

  + Solo ocurren en celulas cardiacas y [neuronas]({{< ref "/pages/neuronas" >}})

[hiperpolarización]({{< ref "/pages/hiperpolarización" >}})

Conducción saltatoria

Pueden ocurrir de una manera [ionica](iones) o metabólica mediante [neurotransmisores]({{< ref "/pages/neurotransmisores" >}})

# Procesos

  + Calcio entra al botón sináptico al sentir un potencial de acción y libera vesiculas de neurotransmitores

  + Abre canales iónicos de la otra neurona
