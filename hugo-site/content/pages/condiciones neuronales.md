---
title: condiciones neuronales
tags:
categories:
date: 2022-10-02
lastMod: 2022-10-03
---
Heminegligencia

  + No reconocer o ser consciente de una mitad del cuerpo

Hemiasomatoagnosia

Hipersomnia

  + Agudización del olfato

    + Afectado por la dopamina

Anosmia

  + Perder el olfato

Hipogeusia

  + Disminusión del sentido del gusto

[sinestesia]({{< ref "/pages/sinestesia" >}})

Afasia

Acromatopsia

  + Pérdida completa del saber, constructo y percepción del color

Epilepsia del lobulo frontal

Anosognosia

  + Nivel de integración de los sistemas neuronales

Agnosia Visual

Prosopagnosia

  + Incapacidad de reconocrer rostros

Hemianomsia

  + No poder ver de un ojo

Ver puntos negros

  + células no ven puntos del espacio o no los registran

Ver luces

  + Fosfenos

  + Existe una hiperactividad neuronal
