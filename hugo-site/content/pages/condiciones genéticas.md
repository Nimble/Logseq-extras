---
title: condiciones genéticas
tags:
categories:
date: 2022-09-29
lastMod: 2022-09-30
---
Condición autosómica es [genética]({{< ref "/pages/genética" >}}), no hereditaria

## Sindrome X frágil
  + Proteina/gen **FMR1** (Fragile Mental Retardation (Protein (**FMRP**)) 1) - Se origina de 5 a 20 repeticiones de la cadena CGG

    + Esta proteina es fundamental en el desarrollo sináptico

    + **Es hereditario**

      + **Las repeticiones de la secuencia aumentan de generación en generación**

      + Las **mujeres** son mucho más **resistentes** pero no inmunes (por duplicidad de cromosoma XX)

      + Ocurre **mayoritariamente** en **hombres** (por la ausencia de otro cromosoma X)

    + Si son más de +20, +100, +200, existen complicaciones en el organismo

      + Siendo muy leves si son más de 20 y llegando a ser extremas como la muerte si son 200

    + El área fragil del cromosoma y el porque del nombre es por la extensión atípica de esta cadena - **La cual de hecho inactiva e inhibe el gen**

  + fenotipo

    + Físicas

      + Similares a las del autismo

      + Marcocefalia

      + Genitales grandes

    + Psicológicas y cognitivas

      + Deficiencia intelectual

      + Deficiencia en lenguaje

      + Concurrente con el Transtorno deficit de atención

      + Concurrente con el ## Transtorno del Espectro Autista


      + Ansiedad

      + Transtorno Obsesivo Compulsivo

      + Impulsividad

      + Fátiga Crónica

## Síndrome de Súperhombre (XYY)

  + Es primordialmente una condición [genética]({{< ref "/pages/genética" >}})

  + fenotipo

    + **Físico**

      + Mayor rapídez en el desarrollo físico

      + Macrocefalia

      + Miden más de 1.80

      + Carencia muscular

      + Temblores

      + Tics

    + **Psicológicas y cognitivas**

      + Alteración en:

        + Lenguaje

        + Aprendizaje

        + Dislexia

      + Desafiante

      + Explosivo

      + Antisocial

      + HIperactividad

      + Impulsivo

  + **Prevalencia**

    + Ocurre en 1 de cada 1000 individuos

  + **Tratamiento**

    + Intervención precoz y temprana con tratamiento hormonal

    + Concientización de los padres

  + **Etiología**

    + Lineas celulares careotipoicas

      + Transmisión genética por [Mosaico Genético]({{< ref "/pages/Mosaico Genético" >}})

## Síndrome de Turner (45-X0)

  + Es un síndrome **genético**

  + Persona con un cromosoma menos (X0) (45) - careotipo 45-X0

  + Sindrome exclusivo en mujeres pues Y0 y hombres sin ese cromosoma no existen o mueren

  + fenotipo

    + **Físico**

      + Linfodemia en extremidades

      + Pezones muy separados

      + Se presentan más de estas caracteristica atípicas en la adolescencia

      + No existe menstruación

      + Baja estatura

      + Cuello ancho o planeado

      + Aorta débil o deteriorada, presión arterial alta, problemas cardiacos

      + Muchas más afectaciones que otros sindromes por la falta de información genética

      + Cataratas

      + Diabetes

      + Pérdida auditiva

    + **Psicológicas y cognitivas**

      + Transtorno Deficit Atención e Hiperactividad (TDAH) - posible

      + Pobre inteligencia lógico-matemática

      + Baja autoestima

  + **Tratamiento**

    + Tratamiento diario de hormonas humanas (crecimiento recombinante)

      + Una de estas hormonas es estrogeno - para llevarlo a niveles regulares

  + **Prevalencia**

    + 1 de cada 2500 mujeres

## Síndrome de Klinefelter (XXY)

  + careotipo - 47-XXY

  + fenotipo

    + **Físico**

      + Testiculos pequeños (aespermia) - Adultos infertiles

      + Agrandamiento de pechos

    + **Psicológicas y cognitivas**

      + Capacidades cognitivas pobres

        + Dislexia

        + Probable deficit de aprendizaje

        + Autoestima baja

  + **Prevalencia**

    + Madres mayores pueden generarlo o son más propensas a

    + 1 de cada 500 hombres

      + Y solo el 32% de estos son diagnosticados

  + **Etiología**

    + Es un sindrome genético

    + Errores en [meiosis]({{< ref "/pages/meiosis" >}}) 1 paterna

    + Errores en [meiosis]({{< ref "/pages/meiosis" >}}) 1 y 2 materna

    + Se diagnostica mediante analisis hormonales y analisis cromosomaticos

    + Aumenta estrogeno, disminuye testosterona

![Error en Meiosis Materna](https://upload.wikimedia.org/wikipedia/commons/0/08/xxy_syndrome.svg)

  + **Tratamiento**

    + Suministración de **testosterona** para llegar a niveles regulares en la **adolescencia**

    + Tratamientos de infertilidad por inseminación invitro

## Transtorno del Espectro Autista
  + Descrito originalmente por Grunya Suchareva. Pero globalmente ignorada o no reconocida #.ol

    + Escrito en ruso 1925

    + Traducción al alemán 1926

    + Traducción al inglés 1996

  + Descrito por Eugene Bleuler

  + Descrito por Leo Kanner en Estados Unidos en 1943 - Le da el nombre de autismo (A uno mismo o en si mismo, ensimismado) - _Ensimismado en su propio mundo_
  + Hans Asperger en 1944 describe el sindrome de asperger (muy similar a las descripciones de Suchareva)

  + **Es un transtorno y no un sindrome porque no hay certeza de que sea de causas únicamente genéticas** - Por esta misma razón no existe un careotipo

  + fenotipo - Como es un espectro los "sintomas" varían enormemente

    + **Físico**

      + Articulaciones y huesos fragiles

      + Pobre coordinación motora

      + Hipotonía - Fibra muscular disminuida

        + Rigidez muscular

        + Hitos de desarrollo motor tardio

      + Postura encorvada

      + Ausencia de *braseo* al andar

        + Caminata anormal

    + **Psicológicas y cognitivas**

      + Hipo e hiper sensibilidad (**sensorial**)

      + No muestra expresiones faciales

      + No participa en juegos interactivos

      + No saben reconocer emociones

      + Aleteo de manos

      + Rutinario

        + Ecolatia: Repiten frases

        + Necesitan tener una rutina y/o patrones de comportamiento - conductas repetitivas

      + Ausencia y retraso en actitud

      + Epilepsia

      + Gran aptitud auditiva

      + Evitación de contacto visual

        + Mirada vacía

      + Interés por objetos pequeños y estimulos **brillantes** y **luminicos**

  + **Tratamiento**

    + Se tiene a **sobrediagnosticar** por el abanico tan grande de sintomas y la falta de enfoque sobre el transtorno

      + Esto patologiza a la población

    + Analisis Conductual Aplicado

    + Suministro de drogas;

      + Antidepresivos

      + Triciclicos

      + Estimulantes

      + Ansioliticos

      + Anticonvulsivos

  + **Etiología**

    + **Autismo Primario** (directo)

      + Es genético y heredado (predisposición)

        + Herencia multifactorial; el fenotipo depende de muchos genes

        + Ajustes del entorno afectan el grado de autismo

    + **Autismo Secundario** (indirecto)

      + Padecimientos que dan paso al autismo

        + Como **Esclerosis Tuberosa**

        + Síndrome de Prader-Will

        + Síndrome de Angelman (microcefalia)

        + Síndrome de Down

        + ## Sindrome X frágil
 - 30% de estos tienen también autismo

        + Neurofibromatosis tipo 1 - Autistas tienen 100x más riesgo de tener esta condición que una persona común

        + Infecciones maternas

      + Ocurre por razones metabólicas (alteración de la fenilectonuria)

  + ### Definiciones de **manual**

    + #### DSM III (1980)

      + Es la primera estandarización del autismo

      + Ocurre en 1980 solo se considera el Autismo Infantil

      + Basado en las descripciones de [Kranner](63375b80-8258-4b2b-bfbe-b4a5b140d217)

      + Ausencia de responsabilidad con otras personas

      + [enmascaramiento]({{< ref "/pages/enmascaramiento" >}}) puede existir

      + Deficit de desarrollo del lenguaje

        + Ecolalia

        + Lenguaje metafórico

        + Inversión pronomial (referirse a uno en 2da o 3ra persona)

        + La mayoría del autismo es prevalente en hombres

          + Es posible que este sesgo ocurra porque las manifestaciones del autismo más estudiadas es la masculina y no las femeninas, entonces es una categorización incompleta

    + #### DSM IV - (1994)

      + Se delimitan multiples formas:

        + Transtorno Autista

        + Transtorno de Rett

          + Congenito y femenino

          + Existen regresiones en la forma de pérdida de habilidades

        + Transorno desintegrativo infantil

        + Transtorno de Asperger

        + Transtorno generalizado del desarrollo no especificado - Descripción: algo raro le ocurre al individuo

    + #### DSM V (2013)

      + Solo existe uno y es el ## Transtorno del Espectro Autista
 (actual)

      + Existe deficiencia de comunicación en varios contextos

        + Por lo mismo ahora existe sobrediagnosticación. casi cualquier comportamiento anormal puede sr consdierado autista

      + Estandares y escalas utilizadas para diagnosticar autismo:

        + ADOS-2: Prueba estandarizada

        + ADIR: Cuestionario/Entrevista a padres de la persona autista

        + AQ: Autodiagnostico en adultos

  + ### Teoría de coherencia central débil

    + Nueva perspectiva al autismo, por Happé y Frith

    + Se origina por un desbalance en la excitación/inhibición [neuronas]({{< ref "/pages/neuronas" >}})al

      + Estudios por Rubenstein y Merzenich 2003
