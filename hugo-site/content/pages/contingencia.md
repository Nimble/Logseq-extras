---
title: contingencia
tags:
categories:
date: 2022-08-30
lastMod: 2022-09-27
---
Contingencia es la _existencia_ de una <ins>relación causal</ins> entre **respuesta** y **reforzador**

  + Se busca que el organismo asigne crédito a su respuesta (causa del reforzador) - if only if

  + Para que la contingencia funcione debe que pasar por un proceso de asignación de crédito y relacionarse de forma causal-proposicional u operacional [condicionamiento operante]({{< ref "/pages/condicionamiento operante" >}})

  + Es externa e instrumental [condicionamiento instrumental]({{< ref "/pages/condicionamiento instrumental" >}})

  + Asignación de crédito es interna al individuo

**Latencia** - Relación temporal: tiempo entre [Respuestas]({{< ref "/pages/Respuestas" >}}) y [reforzamiento]({{< ref "/pages/reforzamiento" >}})(dor)

Contingencias entre respuestas y estimulos provocan aumento en la tasa de respuestas que pueden ser negativas o positivas, [aversiva]({{< ref "/pages/aversiva" >}})s o [apetitiva]({{< ref "/pages/apetitiva" >}})s

Contingencia positivo

  + Conducta **produce** una consecuencia [apetitiva]({{< ref "/pages/apetitiva" >}})

Contingencia negativo

  + Conducta **evita** una consecuencia [aversiva]({{< ref "/pages/aversiva" >}})

[reforzamiento]({{< ref "/pages/reforzamiento" >}})

  + [aversiva]({{< ref "/pages/aversiva" >}}) - Busca evitar una situación aversiva mediante aplicar conductas o quitar obstaculos

  + [apetitiva]({{< ref "/pages/apetitiva" >}}) - Otorga algo deseable

[castigo]({{< ref "/pages/castigo" >}})

  + [aversiva]({{< ref "/pages/aversiva" >}}) - Castiga cada vez que ocurre una conducta indeseable

  + [apetitiva]({{< ref "/pages/apetitiva" >}}) - Quitar algo placentero

  + Negativo - Se basa en la retirada de estímulos positivos

  + Positivo - Reducción de la frecuencia futura de una conducta cuando tras su emisión se presenta un estímulo aversivo

Cantidad y calidad afectan la efectividad

  + Esto esta delimitado por la relevancia de estos estímulos al organismo, es decir la [pertinencia]({{< ref "/pages/pertinencia" >}}) de estos

## Condicionamiento sin contingencias

  + Genera conducta supersticiosa

  + Autoaprendizaje con erronea asignación de crédito

    + No es completamente aleatorio; Existe [contigüidad]({{< ref "/pages/contigüidad" >}}) y cercania pertinente a la conducta supersticiosa ↢ es contingente, o se crea una contingencia falsa o incorrecta
