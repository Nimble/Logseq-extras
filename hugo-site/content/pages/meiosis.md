---
title: meiosis
tags:
categories:
date: 2022-09-29
lastMod: 2022-09-29
---
Solo ocurre para celulas sexuales o haploides

Terminan con la mitad del material genético

  + Sin embargo este material genético es distinto entre las celulas producidas en el proceso (no son copias identicas) - Se le conoce como variedad genética por **entrecruzamiento**

## Eu-disyunción

  + Forma típica

  + Separación y mezcla genética sin errores

  + Se dividen todos los [cromosoma]({{< ref "/pages/cromosoma" >}})s

## No disyunción

  + Puede ocurrir en meiosis 1 o 2

  + Los cromosomas no se separan físicamente en los extremos celulares

  + Si muchos cromosomas no se separan puede que la forma de vida que originada en esa celula no sobreviva

  + Si es ocurre solo en un cromosoma puede que que logre reproducir pero tambien ser fuente o de distintos sindromes genéticos
