---
title: Estructura Neuronal
tags:
categories:
date: 2022-10-02
lastMod: 2022-10-02
---
Varía de especie a especie, lo discutido aquí es referente exclusivamente a la anatomía humana

[Sistema Nervioso Central]({{< ref "/pages/Sistema Nervioso Central" >}})

Sistema Nervioso Periférico

## Glosario

  + #glosario

  + **Sustancia gris**: somas (o cuerpos) de las [neuronas]({{< ref "/pages/neuronas" >}})

  + **Sustancia blanca:** Es la fibra confromada por [axón]({{< ref "/pages/axón" >}})es
  + Ley de Valiey

    + Ventral
      + Referente a lo motor

    + Dorsal
      + Ejes sensoriales

  + Ganglios

    + Conjunto de somas en el Sistema Nervioso Periférico

  + Fisículos

    + Conocidos como **tractos**

    + Conjunto de [axón]({{< ref "/pages/axón" >}})es en el [Sistema Nervioso Central]({{< ref "/pages/Sistema Nervioso Central" >}})

  + Nervios
    + Conjunto de [axón]({{< ref "/pages/axón" >}})es en el Sistema Nervioso Periférico

  + Núcleos

    + Conjunto de somas en el [Sistema Nervioso Central]({{< ref "/pages/Sistema Nervioso Central" >}})

  + Conexinas

    + Conocidas como *gap junctions*

  + ### [condiciones neuronales]({{< ref "/pages/condiciones neuronales" >}})

  + Si las **proteínas** terminan en *asa* son todas enzimas

  + **Infraregulación**
    + Tambien conocido como down regulation

    + Los receptores neuronales dejan de estar expuestos en las neuronas, efectivamente cerrandola o haciendola inmune a este tipo de señales

  + Huso

    + Es un giro fusiforme

![Huso Muscular](https://img.yasalud.com/uploads/2011/12/husos-musculares.jpg)

## Nociones clave

  + La sangre es neurotoxica - es decir mata y/o daña las [neuronas]({{< ref "/pages/neuronas" >}})

    + Por lo tanto se debe que presta gran atención a los vasos

## Datos curiosos

  + #nota-propia La [sinestesia]({{< ref "/pages/sinestesia" >}}) es su propia experiencia de vida, es decir es una normalidad porque es su percepción común, y es así también uno de los mejores ejemplos de la qualia y la variación de perspectiva incluso en organismos de la misma especie. Esto implica que no existe una objetividad real a un nivel perceptivo/sensorial

  + #nota-propia El reconocimiento de rostros no es exclusivamente humano, también lo hacen los perros ¿Porqué y cómo?

  + Gamma ray therapy operations

  + Se diseña en México un casco que enfría el cerebro
