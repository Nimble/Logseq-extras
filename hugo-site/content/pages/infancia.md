---
title: infancia
tags:
categories:
date: 2022-08-31
lastMod: 2022-08-31
---
En los primeros 5 años de vida el aprendizaje es masivo y la plasticidad cerebral se ve muy implicada en este proceso pues el infante aprende literalmente todo sobre su entorno.


