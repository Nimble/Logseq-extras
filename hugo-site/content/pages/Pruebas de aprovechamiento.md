---
title: Pruebas de aprovechamiento
tags:
categories:
date: 2022-08-29
lastMod: 2022-08-30
---
**Todas las preguntas y respuestas deben ser sintetizadas y concisas**

Se corta la monotonía y regana atención al cambiar los tipos de reactivo

Distractores deben estar siempre arriba o abajo de la respuesta correcta

  + 5 opciones reales por 2 falsas o erroneas
