---
title: influencias
tags:
categories:
date: 2022-10-02
lastMod: 2022-10-02
---
## Directa

  + ### Autoridad

    + Grado de cercanía de figuras de autoridad modifica su efecto en la alteración conductual

    + #### Autorización
      + Liberación de responsabilidad en el "autorizado"

    + #### Rutinización
      + Serie de conductas aversas se convierte solo en un día más de desensibilización

      + No se analiza la conducta y se normaliza

      + Lleva a la deshumanización y pérdida de empatía - Por la falta de contacto o cercanía (en todo sentido)

  + ### Obedencia

    + Requiere de una autoridad

    + Seguir ordenes de una figura de autoridad

    + 

## Indirecta

  + ### Conformidad
    + Presión social

      + Conductas que llevan los demás ↣ demanda acoplación y acatación al grupo

      + Cambia ideas ↣ actividades ↣ conducta

      + Puede surgir de influencias **directas** o **indirectas**

      + Por el sentido de "deuda" (retribución, compromiso)

    + #nota-propia ¿Porqué es díficil oponerse o no estar de acuerdo con el otro (sin confianza) y con una mayoría? - ¿Será que los juicios convergen o divergen? - Es el efecto de un marco de referencia ajeno que afecta el mundo individual

    + La **privacidad** reduce en gran medida el impacto de la conformidad

  + ### Complaciencia
    + No es una orden directa, es una actitud o poder que una persona o un colectivo tiene sobre otro(s)

    + No necesita ser proveniente de una figura de autoridad

    + Conducta se adopta a petición de otros (incluso indirecta)

      + Se acomoda acorde a signos y lenguaje de otras personas

      + **Coacción**: acciones influyentes como miradas

    + No es necesario acoplarse a esas ordenes, pero se siguen de cualquier manera

    + Palabra clave: **ceder**

  + [Influencia Social]({{< ref "/pages/Influencia Social" >}})

    + Reciprocidad

      + Evocación de una acción en el otro a partir de una propia

    + ### Validación social


    + Escacez
      + Carencia de objetos

    + Compromiso

      + Social(?)
