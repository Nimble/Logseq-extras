---
alias:
- neuronal
- neurona
title: neuronas
tags:
categories:
date: 2022-08-31
lastMod: 2022-10-02
---
Neuronas Binoculares: Son formadas solo y únicamente cuando se utilizan ambos ojos, esto implica que la abstracción incluso del conocimiento (en todo sentido) y la capacidad cereblar ↣ **No son innatas**

Las neuronas tienen solo un [axón]({{< ref "/pages/axón" >}}) que es bifurcable

La membrana celular de la neurona son semipermeables

  + Utiliza **fosfolípidos** en constante movimiento, estos bloquean el paso libre de substancias y iones

  + La [célula]({{< ref "/pages/célula" >}}) intenta adquirir [homeostasis]({{< ref "/pages/homeostasis" >}}) por gradiente de concentración

    + Para esto **expele** o **adquiere** iones

  + Sin embargo dentro de la neurona existen canales ionicos que permiten el paso de estos

    + El estado normal o de **reposo** es que estén cerrados en el rango de -70μV (-70 microVoltios)

      + Las neuronas en un organismo vivo nunca están en reposo, estas mediciones solo ocurren cuando la celula esta muerta o en laboratorio

[sinapsis]({{< ref "/pages/sinapsis" >}})
