---
title: Técnicas Neurológicas
tags:
categories:
date: 2022-10-02
lastMod: 2022-10-02
---
Buscan estudiar la sensopercepción

## Psicofísica

  + Vincula estímulos con sensaciones

  + Ejemplo: Frío-malestar

  + Las características del estimulo y sus sensaciones **no son lineales**

  + Personas tienen distintos [umbral sensorial]({{< ref "/pages/umbral sensorial" >}})

## Neuroanatomía

  + Cerebro con surcos muy pronunciados es un cerebro muy **degenerado**, con una substancial pérdida de neuronas

  + La tinta de Golgi tiñe unicamente el 5% de las células

## Electrofisiología
  + Registros (mediciones) unicelulares (dentro de la neurona) y multicelulares (a una población o grupo de neuronas) - Estas son medidas directas

  + Usa electrodos

  + Los tipos de respuesta de los potenciales de acción se ven distintos dependiendo si es un registro multi o unicelular

  + Se usan métodos de medición **invitro**

    + Registro e inyección de corriente electrica

    + Se aislan las células del organismo

  + Método **invivo**

    + Es una operación intracraneal

    + Se requiere un objetivo clínico por lo invasivo del método

    + Ocurre dentro del organismo vivo

    + **Electrocorticografía**

      + Abre la cabeza y conecta electrodos

      + Se usa para la epilepsia y tumores

    + **Estereotaxico**

      + Electrodos profundos

  + ### Electroencefalografía

    + Registros extracraneales

    + Actividad electrica de poblaciones de neuronas (áreas)

    + Se usan programas y modelos matemáticos para la interpretación de datos

  + ### Estimulación Electrica

    + Responde la pregunta ¿Qué sicede si se excita cierta área del cerebro?

    + Manda pulso de corriente al tejido

      + Se intentan simular corrientes naturales

## Farmacología

  + Distinto a la ## Electrofisiología
 pero solo en el sentido de que en vez de usar electrodos y electricidad se usan químicos

  + #### In(activación) cerebral

    + Se inhibe farmacologicamente, con lesiones o temperaturas gélidas

      + Se usa helio

    + Los [procesos neuronales](Sistemas Neuronales) se pausan

## Estimulación Magnética Transcraneal

  + Transcraneal Magnetic Stimulation (TMS)

  + El cerebro genera un campo electromagnético

  + Es focalizado

  + No ha sido estudiado experimentalmente

  + Modifica el espectro u ondas electromagneticas encotnradas naturalmente en el [cerebro]({{< ref "/pages/cerebro" >}})

## Neuroimagen

  + Es una medida indirecta del funcionamiento de las neuronas

  + Imagen de Resonancia Magnetica funcional (fMRI)

  + Se busca el entendimiento de la actividad metabólica

  + Tomografía por Emisión de Positrones (PET)

  + Hay interveción analítica computacional

## Optogenética

  + Técnica celular invivo con animales ejecutando tareas (o no)

  + Se administra un virus y se implanta un LED

  + Es muy invasiva; solo se usa en animales como ratas

  + Permite discriminar las neuronas a observar, es decir saber el tipo de la ruta metabólica

  + Longitud de onda de una luz determina la inhibición o activación de los virus implantados

    + El virus códificado se una a los canales y responde a la luz, y este mismo deja o no pasar los impulsos
