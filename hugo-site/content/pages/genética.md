---
title: genética
tags:
categories:
date: 2022-09-27
lastMod: 2022-09-30
---
La diferencia principal entre la herencia y las condiciones genéticas es que la herencia proviene de un patrón genético portado pr los padres mientras que las condiciones geneticas son afectaciones que no cargan los padres y usualmente ocurren en la [meiosis]({{< ref "/pages/meiosis" >}}). Esto quiere decir que los problemas genéticos son independientes de los padres, mientras que los hereditarios no.

cromatina - Cadea completa de cromosomas

  + Previo a [meiosis]({{< ref "/pages/meiosis" >}}) y [mitosis]({{< ref "/pages/mitosis" >}}) se divide en cromosomas

careotipo: Arreglo y composición en pares de cromosomas

[cromosoma]({{< ref "/pages/cromosoma" >}}): clasificaciones y divisiones naturales del genoma, ADN o cromatina

Redundancia genética

  + Los hombres al no tener esta redundancia (XY) son más propensos a padecer condiciones géneticas ligadas al sexo.

    + El otro cromosoma X femenino se utiliza como suplente de la información genética erronea o faltante

    + Razón tambien de porque las mujeres son generalmente más sanas y viven más tiempo

gen: Bloque secuencia o código

  + Su nombramiento depende de su localización en el genoma

  + Es ordenado y descrito por su localización

    + Región P (pertite (pequeña))

    + Esta dividido en forma de bandas

  + Un alelo es el gen individual o caracteristico de la persona (instanciado particular)

  + No existe una relación uno a uno entre genotipo y la expresión de este fenotipo

    + Multiples genes alteran multiples expresiones, es muy díficil marcar delimitaciones únicas o caracteristicas

  + Cada secuencia es diferente, esto define al ADN. Todos contenemos los mismos _tipos_ de genes, pero no la misma información genética

    + #nota-propia esto implica que es programable pues es una secuencia aritmetica (?) - Así como un sistema binario computacional

    + El ADN es ordinal; el orden de la secuencia importa y el contenido es lo que afecta la variación del organismo

[condiciones genéticas]({{< ref "/pages/condiciones genéticas" >}})

[Mosaico Genético]({{< ref "/pages/Mosaico Genético" >}})
