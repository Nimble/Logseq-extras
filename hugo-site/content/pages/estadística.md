---
title: estadística
tags:
categories:
date: 2022-08-31
lastMod: 2022-09-26
---
Curva normal (campana de Gauss)

  + Deslinda → Término típico y atípico (etiquetas: normal y anormal)

    + Connotaciones:

      + Anormal genera extrañeza y cierta aversión

      + Atípico suena  a fuera del margen o especial

      + ¿Porqué lo atípico, extraño, raro, anormal tiene inherentemente connotaciones negativas?

[correlación]({{< ref "/pages/correlación" >}}) dentro de la estadística se refiere al grado de relación entre variables

  + Lo esperado para el total es .70

  + Lo esperado para cada reactivo es .20

## Estandarización

### Metanalisis: ¿Qué medir?

  + Revisión crítica y extensiva de la literatura

  + Encuentra indicadores

  + Genera un plan de prueba

¿En/a quien se quiere medir?

Determina y delimita áreas de influencia

¿Cómo se están comunicando los sujetos?

  + Usa asociación libre

  + Grupos focales

  + Redes semanticas

  + Debe tomarse en cuenta escolaridad

  + Lleva al [análisis factorial]({{< ref "/pages/análisis factorial" >}}) (analisis multidimensional)

    + Matrices y tablas - dimensiones-formas-aspectos

¿Para qué lo quiero medir?

  + Diagnostico

  + Clasificación

  + Conocer el constructo
