---
title: Sistema Visual
tags:
categories:
date: 2022-10-02
lastMod: 2022-10-03
---
Talamo
 → **Centro geniculado lateral** es donde se proces toda la información visual

<u>Así como la percepción física tambien la interpretación de las imágenes variará de especie a especie</u>

## Cualidades físicas de la Luz

  + No existe visión sin luz

  + Hace uso de un proceso de transducción

    + Se traducen los estimulos fotolumínicos a señales electromagnéticas y químicas

  + Reflexión de la luz

    + Otorga el calor y el reflejo

  + Absorción

    + Capacidad de un objeto para permitir el paso de distintas ondas en el espectro visible

  + Refracción

    + Modifica el angulo que tiene el rayo de luz

  + ### Espectro electromagnético

    + Longitudes de onda fuera del espectro visible, no hay receptores ni significancia cerebral (no se sabe/puede interpretar esas señales incluso si nuestros ojos las vieran)

    + #nota-propia ¿Porqué se crea una esfera de color (inicia y termina con el violeta, si se consideran los extremos rojo y azul)? Aquí hay muchas implicaciones y tendencias a colores nuevos, es decir colores que trascienden el espectro que vemos los humanos. Imagina más allá del azul, el rojo y el violeta.

## Anatomía

![diagrama ocular](https://terraceeyecentre.com.au/wp-content/uploads/2017/05/anatomy-of-the-eye-terrace-eye-centre-brisbane-1.png)

  + El ojo esta lleno de Nervios
 y de vasos

  + Cascadas o sacudidas - son movimientos oculares para enfocar

  + Todas las moleculas que reaccionan a la luz se llaman **opsinas**

  + Musculo recto laterial/medial

    + Enerva el ojo

    + Depende de [neurotransmisores]({{< ref "/pages/neurotransmisores" >}}) motores

  + Cornea ↣ humor acuoso (líquido) ↣ iris

    + #nota-propia ¿Cúal es la función de que el iris este pigmentado?

  + Iris

    + Siempre esta pigmentado (es un músculo colorado)

    + Se contrae y dilata por la Noradrenalina
 - esta relacionado con la euforia?

  + Lente cristalino

    + Es biconvexo

    + Es felxible y contiene musculos

    + Son los que enfocan y modifican el humor acuoso

    + Se mueve para que todos los rayos caigan o lleguen a la Fovea


  + Humor acuoso

    + Tambien conocido como vitruoso - humor vitreo

    + El líquido espeso da forma al relleno

  + ### Retina
    + Son [neuronas]({{< ref "/pages/neuronas" >}}) alrededor de todo el globo ocular

    + Conos y bastones no estan dispuestos de manera aleatoria ni uniforme, pero al mismo tiempo es particular al individuo y no estandarizada

    + #### Conos
      + Existen **pocos** de estos en la **periferia** de la retina

      + Su enfoque son los detalles y colores

        + No se puede leer con la retina periferica - se tiene que enfocar y hacer uso de conos

      + Abundan en la retina central

      + Se adaptan mucho más lentamente a los cambios luminicos

      + Tienen 3 moleculas distintas sensibles al color (una por cada uno "principal")

        + Cono azul

        + Cono verde

        + Cono rojo

        + Entre más conos de algún color haya la sensibilidad a ese color o relacionados de la persona variará

        + Sin embargo el que es el color es una arbitrariedad completa encuanto a sus cualidades

      + Son afectados por hormonas

      + Son casi inutiles en la oscuridad, funcionan mejor con luz

      + Existen 3 millones por ojo

    + #### Bastones
      + Ausentes en la Fovea


      + Hay **muchos** en la **perifería** de la retina

      + Encargados de codificar y ser especialmente sensibles al movimiento

      + Tienen una molecula sensible a la luz llamada **rodopsina** (rod(baston-psina))

      + Son especializados en percibir cambios luminicos de intensidad pero no en las longitudes de onda (colores)

      + No tienen problemas en operar en la oscuridad

        + Amplifican los fotones

        + Sensibles al cambio duradero y largo - no hacen una buena integración de los colores

        + Generan imagenes acromáticas (sin color?)

      + Hay aproximadamente 100 millones por cada ojo

    + Tiene 3 capas

      + Solo la capa final de conos y bastones son celulas transductoras llamadas también fotoreceptores (energía lumninica pasa a un lenguaje biológico y neuronal)

      + Capa nuclear (interna)

        + células horizonatales

          + Guardan información y comunica fotoreceptores

        + Son celulas bipolares

        + Las células anacrinas integran y conectan las bipolares y las ganglionales

      + Capa plexiforme externa (media)

      + Capa más externa

        + Formada de celulas ganglionales, estos a su vez forman

        + Forma el [nervio](Nervios
) óptico

        + Puede comunicar potencial de acción - ninguna de las otras capas y celulas de la retina lo hacen

        + Información del fotoreceptor va hacia las gangloniales (capa más externa)

  + Quiasma óptico

    + Entrecruzamiento de la ### Retina
 Nasal

    + Son las fibras del nervio optico que se cruzan a la altura de la nariz

  + Esclorotica

    + Es la sustancia blanca en el ojo

  + Fovea
    + Depresión dentro del ojo, es una endidura

    + Su posición es el centro **horizontal** y **vertical** de la ### Retina


    + Es el punto donde la imagen se "imprime" o se codifica y se inicia la ## Neurociencia del sistema visual


    + No existen #### Bastones
 en esta área

  + Punto ciego

    + También conocido como disco óptico

    + Un punto de la ### Retina
 no codifica el color y no existen celulas fotoreceptoras

    + El cerebro neurológicamente y/o computacionalmente rellena esa información

  + Campos receptivos

    + Area del espacio que codifica el marco visible al recibir estímulos apropiados

    + La ### Retina
 codífica una parte del espacio (lo visible)

    + Es un mapa en la ### Retina
 de todo lo que se percibe visualmente

    + **En otras palabras, todo lo que vemos esta en nuestros ojos "una repesentación espacial retinal"**

## Neurociencia del sistema visual
  + **Luz** genera **proteina g inhibidora** la cual **cierra** el canal ↣ [hiperpolarización]({{< ref "/pages/hiperpolarización" >}})

    + [sinapsis]({{< ref "/pages/sinapsis" >}}) ocurre en el nucleo geniculado lateral

      + Ahí se procesa toda la información y es parte del Talamo


      + Las sinapsis que ocurren en la ### Retina
 está organizado por capas y la información no se procesa a ese nivel exterior, solo se recibe

  + Los canales de sodio están abiertos en penumbra - Despolarizado - Gmp-Ciclico presente

    + Al activarse los fotoreceptores por la luz se remueve esa ciclicidad y se inactiva el sistema nervioso

    + GTP (Guanosin TriPhosphate) se libera y desintegra al GMPc (Guanosin MonoPhosphate-ciclico)

      + Se reduce a una molecula con 2 fosfatos (fosfodiesterasa) la cual adquiere la parte ciclica del GMP

      + GTP ↣ ODP ↣ Fosfodiesterasa

  + Se liberan [neurotransmisores]({{< ref "/pages/neurotransmisores" >}}) en la penumbra

  + Vitamina A es crucial en la visión

  + Toda la suma de las señales electricas en la retina llegan al nervio óptico y genera un potencial de acción en los ganglios

  + ### La ruta visual

    + Primer relevo sináptico - la ### Retina
 es una extensión neuronal que termina en la corteza occipital (tambien conocida como corteza estriada porque se pliega la información de un ojo o de otro - la separación visual existe hasta este punto)

    + La ### Retina
 tiene bastantes rutas

      + Colículos superiores ↣ Núcleo pulvinar ↣ amigdala

        + Una amigdala calcificada puede no reconocer miedo en ninguna forma, esto implica rostros con miedo

      + Via lateral

      + Lobulo temporal - Asa de Meyer ↢ axones visuales pasan por ahí

  + ### Ruta visual de los #### Bastones

    + Ganglios cambian en si interior a positivo

      + Campos receptivos de actividad e inactividad

        + Hacen a la celula bipolar responder

        + Es un lenguaje entre activación y desactivación

          + Como el binario 1/0 y/o el codigo morse

        + La CODIFICACIÓN depende de los campos receptivos

          + Centro off ↣ apaga anillo - inversamente proporcional

          + Centro on ↣ prende el anillo

            + Dirige las tasas de disparo ↣ a donde se dirige la luz

          + Campo receptivo en el [cerebro]({{< ref "/pages/cerebro" >}}) tambien esta codificado

          + #nota-propia Es lo que genera los bordes en las imágenes

            + La contiguidad receptiva celular

              + El porque de las comparaciones de color aparenta colores distintos cuando son el mismo

  + ### Ruta visual de los #### Conos


    + Ocurre lo mismo que en los [bastones](### Ruta visual de los ((633a6938-92cb-4af3-b01f-a5e5c28518eb))
) solo que el centro no solo es receptivo a la luz sino a algún color básico (RGB)

      + Tambien por eso depende de la luz

      + Mantener la vista fija en un punto

        + Estimula a un tipo particular de células lo cual cierra los canales

          + Genera **postimagen**

            + Es como quemar la retina/pantalla

        + células estimuladas con un color específico

          + Visualmente usa el complementario o contrario al color visto

          + células activadas ↣ inhibidas forman imagenes falsas a partir de un potencial de acción de las celulas ganglionales

## Percepción visual

  + Los ojos funcionan en opuestos diagonales

    + Las secciones "temporales" de las ### Retina
s no son opuestas sino **directas**

    + ### Retina
 Nasal crea el quiasma óptico

## Aflicciones Visuales

  + No son [condiciones neuronales]({{< ref "/pages/condiciones neuronales" >}}), pero sí son [condiciones genéticas]({{< ref "/pages/condiciones genéticas" >}}) y hasta hereditarias

  + Se exploran los ojos y visión para comprobar daño cerebral

    + Movimiento ocular alterado y lento - se desbalancea

  + Si la imagen no se imprime de manera correcta en la Fovea
 puede causar distintos problemas como:

  + Miopia
    + Se ve borroso porque la imagen se "imprime" antes de llegar a la Fovea
 y ((633a633d-f9cc-43ef-b7e4-8afb0039bc6f)), es decir los rayos de luz coinciden de manera correcta pero no en el punto en el que deberían.

    + Eso causa un traslape y duplicación de la imagen que el [cerebro]({{< ref "/pages/cerebro" >}}) no sabe/puede corregir, lo cual causa la visión borrosa

  + Astigmatismo

    + La incidencia de luz por la cornea se ve alterada lo cual causa un desajuste (o error) en el ángulo de visión

  + Hipercromatopia

    + Ojo chico coverge atras de la retina (opuesto a la Miopia
)

    + Presenta condición de "ojos cansados"

## Experimentos #experimentos

  + Paul Bach Irrita
