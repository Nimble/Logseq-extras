---
title: error de medición
tags:
categories:
date: 2022-09-04
lastMod: 2022-09-04
---
Error Constante o Sistematico

  + Afecta validez

  + Es un error generalizado [normativas]({{< ref "/pages/normativas" >}})

  + Falla en el método (aplicación) o instrumentos

Error Variable o Aleatorio

  + Afecta [confiabilidad]({{< ref "/pages/confiabilidad" >}})

  + Es un error inesperado, no contemplado o fuera del dominio y control del investigador

    + Pueden ser pensamientos y constructos de la persona desconocidos para el aplicador y diseñador

Error Estandar de la Media

  + Se refiere a la curva [estadística]({{< ref "/pages/estadística" >}})

  + Afecta aproximaciones [estadística]({{< ref "/pages/estadística" >}})s

  + Afecta a la anticipación del error variable

Fuentes de error

  + Instrumental

    + Muestreo del reactivo

    + Tamaño de la muestra

  + Sujeto

    + **Adivinación**: El sujeto deduce que es lo que el instrumento esta midiendo y acopla sus respuestas a esto

    + **Interpretación inadecuada**: No entender los reactivos

    + **Fátiga**: Sujeto en una situación no apremiante para contestar los reactivos

      + Se debe saber cuando medir y cuando el sujeto es receptible

    + **Errores humanos**: Símil. Fuera del control del diseñador
