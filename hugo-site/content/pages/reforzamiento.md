---
title: reforzamiento
tags:
categories:
date: 2022-09-04
lastMod: 2022-09-27
---
Positivo - Busca que una conducta aumente su frecuencia

Negativo - Busca evitar una situación aversiva mediante aplicar conductas o quitar obstaculos

## Reforzadores secundarios o condicionados
  + Hace uso de asociación entre estimulos para generar conductas, en especial el uso de demoras o latencias

Dependiendo de la [pertinencia]({{< ref "/pages/pertinencia" >}}) la retroalimentación verbal puede ser considerado un reforzador

## Programas de reforzamiento intermitente

  + ### Programas de rázon [contingencia]({{< ref "/pages/contingencia" >}})

    + Favorecen Tiempos Entre Respuestas **corto**

    + **Variable**

      + Se desconoce el número de acciones necesarias para obtener el reforzador

    + **Fijo**

      + Cantidad requerida o fija de respuestas para obtener un reforzador

      + **Razón 1:1** Se le conoce como reforzamiento continuo

      + Hace uso del Registro acumulativo: Tasa de respuestas - tensión de razón - carrera de razón - respuestas necesarias

      + Se usan pausas pre/post reforzamiento

      + Se presenta **festoneo**

  + ### Programas de intervalo

    + Favorecen Tiempos Entre Respuestas **largo**

    + **Variable**

      + Sin festoneo

      + Es constante

      + Se desconoce la cantidad de tiempo que pasará

    + **Fijo**

      + Se presenta **festoneo**

      + Se da o se requiere que el organismo genere una respuesta despues de un tiempo dado(fijo) y se le otorga un reforzador

  + Los reforzadores no siempre están disponibles

  + Se refuerzan tiempo entre respuestas cortas

    + Lo cual genera que los tiempos entre respuestas cortos sean más probables en el futuro
